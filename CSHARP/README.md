## OENIK_PROG3_2019_2_FZ473F

---

## About this directory

The **CSHARP** directory has been created and committed to the repository.

*This directory contains all of the C# program parts/files for the initial half year project for [Webprogramming and advanced development techniques](http://users.nik.uni-obuda.hu/prog3/) subject.*

---

## About the QUFLEET Application

The QUFLEET application is a comprehensive, "real-time" fleet and inventory managing software that is using the QUJET Airline's database to query needed pieces of information, create new instances, modify existing records and much more...

**The basic program's functions are:**

1.  List items from the database,
2.  Add items to the database,
3.  Modify items in the database,
4.  Delete items from the database,
5.  Get the details of a plane,
6.  Summarized reports on available airports,
7.  Abstract details of current employees,
8.  Complete statement of a booking,
9.  Can book flights for passengers using the [Java Web Application](../JAVA/).