﻿// <copyright file="AirportsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the AIRPORTS's table CRUD methods.
    /// </summary>
    public class AirportsRepository : IAirportsRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreateAirport(AIRPORT airport)
        {
            this.db.AIRPORTS.Add(airport);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteAirport(int id)
        {
            this.db.AIRPORTS.Remove(this.db.AIRPORTS.First(x => x.AIRPORT_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<AIRPORT> GetAll()
        {
            return this.db.AIRPORTS;
        }

        /// <inheritdoc />
        public AIRPORT GetOne(int id)
        {
            return this.db.AIRPORTS.First(x => x.AIRPORT_ID == id);
        }

        /// <inheritdoc />
        public void UpdateFAA(int id, string faa)
        {
            this.db.AIRPORTS.First(x => x.AIRPORT_ID == id).FAA = faa;
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void UpdateIATA(int id, string iata)
        {
            this.db.AIRPORTS.First(x => x.AIRPORT_ID == id).IATA = iata;
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void UpdateICAO(int id, string icao)
        {
            this.db.AIRPORTS.First(x => x.AIRPORT_ID == id).ICAO = icao;
            this.db.SaveChanges();
        }
    }
}
