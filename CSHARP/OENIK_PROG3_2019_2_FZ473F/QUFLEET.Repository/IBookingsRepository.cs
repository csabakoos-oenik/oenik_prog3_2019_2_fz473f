﻿// <copyright file="IBookingsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The BOOKINGS's CRUD operations.
    /// </summary>
    public interface IBookingsRepository : IRepository<BOOKING, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Create a new booking and adds it to the BOOKINGS table.
        /// </summary>
        /// <param name="booking">The new booking.</param>
        void CreateBooking(BOOKING booking);

        // delete

        /// <summary>
        /// Deletes the selected booking from the BOOKINGS table.
        /// </summary>
        /// <param name="id">The booking's id.</param>
        void DeleteBooking(int id);
    }
}
