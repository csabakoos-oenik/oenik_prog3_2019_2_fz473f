﻿// <copyright file="FlightAttendantsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the FLIGHT_ATTENTANDS's table CRUD methods.
    /// </summary>
    public class FlightAttendantsRepository : IFlightAttendantsRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreateEmployee(FLIGHT_ATTENDANTS employee)
        {
            this.db.FLIGHT_ATTENDANTS.Add(employee);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteEmployee(int id)
        {
            this.db.FLIGHT_ATTENDANTS.Remove(this.db.FLIGHT_ATTENDANTS.First(x => x.EMPLOYEE_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<FLIGHT_ATTENDANTS> GetAll()
        {
            return this.db.FLIGHT_ATTENDANTS;
        }

        /// <inheritdoc />
        public FLIGHT_ATTENDANTS GetOne(int id)
        {
            return this.db.FLIGHT_ATTENDANTS.First(x => x.EMPLOYEE_ID == id);
        }

        /// <inheritdoc />
        public void UpdateLanguages(int id, short languages)
        {
            this.db.FLIGHT_ATTENDANTS.First(x => x.EMPLOYEE_ID == id).LANGUAGES = languages;
            this.db.SaveChanges();
        }
    }
}
