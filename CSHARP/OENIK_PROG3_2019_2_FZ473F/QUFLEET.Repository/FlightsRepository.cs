﻿// <copyright file="FlightsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the PILOTS's table CRUD methods.
    /// </summary>
    public class FlightsRepository : IFlightsRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreateFlight(FLIGHT flight)
        {
            this.db.FLIGHTS.Add(flight);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteFlight(int id)
        {
            this.db.FLIGHTS.Remove(this.db.FLIGHTS.First(x => x.FLIGHT_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<FLIGHT> GetAll()
        {
            return this.db.FLIGHTS;
        }

        /// <inheritdoc />
        public FLIGHT GetOne(int id)
        {
            return this.db.FLIGHTS.First(x => x.FLIGHT_ID == id);
        }

        /// <inheritdoc />
        public void UpdatePrice(int id, int price)
        {
            this.db.FLIGHTS.First(x => x.FLIGHT_ID == id).PRICE = price;
            this.db.SaveChanges();
        }
    }
}
