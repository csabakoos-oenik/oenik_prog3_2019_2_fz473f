﻿// <copyright file="IFlightsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The FLIGHTS's CRUD operations.
    /// </summary>
    public interface IFlightsRepository : IRepository<FLIGHT, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a new flight and adds it to the FLIGHTS table.
        /// </summary>
        /// <param name="flight">The new flight.</param>
        void CreateFlight(FLIGHT flight);

        // update

        /// <summary>
        /// Updates the selected flight's price.
        /// </summary>
        /// <param name="id">The flight's id.</param>
        /// <param name="price">The flight's new price.</param>
        void UpdatePrice(int id, int price);

        // create

        /// <summary>
        /// Deletes the selected flight from the FLIGHTS table.
        /// </summary>
        /// <param name="id">The flight's id.</param>
        void DeleteFlight(int id);
    }
}
