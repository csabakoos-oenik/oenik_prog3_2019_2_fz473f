﻿// <copyright file="MaintenanceCrewRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the MAINTENANCE_CREW's table CRUD methods.
    /// </summary>
    public class MaintenanceCrewRepository : IMaintenanceCrewRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreateEmployee(MAINTENANCE_CREW employee)
        {
            this.db.MAINTENANCE_CREW.Add(employee);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteEmployee(int id)
        {
            this.db.MAINTENANCE_CREW.Remove(this.db.MAINTENANCE_CREW.First(x => x.EMPLOYEE_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<MAINTENANCE_CREW> GetAll()
        {
            return this.db.MAINTENANCE_CREW;
        }

        /// <inheritdoc />
        public MAINTENANCE_CREW GetOne(int id)
        {
            return this.db.MAINTENANCE_CREW.First(x => x.EMPLOYEE_ID == id);
        }

        /// <inheritdoc />
        public void UpdateSpecialization(int id, string specialization)
        {
            this.db.MAINTENANCE_CREW.First(x => x.EMPLOYEE_ID == id).SPECIALIZATION = specialization;
            this.db.SaveChanges();
        }
    }
}
