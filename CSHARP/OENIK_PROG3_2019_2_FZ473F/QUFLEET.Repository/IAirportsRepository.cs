﻿// <copyright file="IAirportsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The AIRPORTS's CRUD operations.
    /// </summary>
    public interface IAirportsRepository : IRepository<AIRPORT, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a new airport and adds it to the AIRPORTS table.
        /// </summary>
        /// <param name="airport">The new airport.</param>
        void CreateAirport(AIRPORT airport);

        // update

        /// <summary>
        /// Update's the selected airport's IATA identifier.
        /// </summary>
        /// <param name="id">The airport's ID.</param>
        /// <param name="iata">The new IATA identifier.</param>
        void UpdateIATA(int id, string iata);

        /// <summary>
        /// Update's the selected airport's ICAO identifier.
        /// </summary>
        /// <param name="id">The airport's ID.</param>
        /// <param name="icao">The new IATA identifier.</param>
        void UpdateICAO(int id, string icao);

        /// <summary>
        /// Update's the selected airport's FAA identifier.
        /// </summary>
        /// <param name="id">The airport's ID.</param>
        /// <param name="faa">The new IATA identifier.</param>
        void UpdateFAA(int id, string faa);

        // delete

        /// <summary>
        /// Deletes the selected airport from the AIRPORTS table.
        /// </summary>
        /// <param name="id">The airport's ID.</param>
        void DeleteAirport(int id);
    }
}
