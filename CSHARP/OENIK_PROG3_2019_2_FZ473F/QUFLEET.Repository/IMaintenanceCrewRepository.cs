﻿// <copyright file="IMaintenanceCrewRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The MAINTENANCE_CREW's CRUD operations.
    /// </summary>
    public interface IMaintenanceCrewRepository : IRepository<MAINTENANCE_CREW, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a employee and adds it to the MAINTENANCE_CREW table.
        /// </summary>
        /// <param name="employee">The new employee.</param>
        void CreateEmployee(MAINTENANCE_CREW employee);

        // update

        /// <summary>
        /// Updates the selected employee's specialization.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        /// <param name="specialization">The employee's new specialization.</param>
        void UpdateSpecialization(int id, string specialization);

        // delete

        /// <summary>
        /// Deletes the selected employee from the MAINTENANCE_CREW table.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        void DeleteEmployee(int id);
    }
}
