﻿// <copyright file="EmployeesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the EMPLOYEES's table CRUD methods.
    /// </summary>
    public class EmployeesRepository : IEmployeesRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreateEmployee(EMPLOYEE employee)
        {
            this.db.EMPLOYEES.Add(employee);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteEmployee(int id)
        {
            this.db.EMPLOYEES.Remove(this.db.EMPLOYEES.First(x => x.EMPLOYEE_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<EMPLOYEE> GetAll()
        {
            return this.db.EMPLOYEES;
        }

        /// <inheritdoc />
        public EMPLOYEE GetOne(int id)
        {
            return this.db.EMPLOYEES.First(x => x.EMPLOYEE_ID == id);
        }

        /// <inheritdoc />
        public void UpdateName(int id, string name)
        {
            this.db.EMPLOYEES.First(x => x.EMPLOYEE_ID == id).NAME = name;
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void UpdateSalary(int id, int salary)
        {
            this.db.EMPLOYEES.First(x => x.EMPLOYEE_ID == id).SALARY = salary;
            this.db.SaveChanges();
        }
    }
}
