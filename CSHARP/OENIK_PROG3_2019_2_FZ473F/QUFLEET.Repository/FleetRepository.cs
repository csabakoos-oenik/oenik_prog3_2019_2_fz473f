﻿// <copyright file="FleetRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the FLEET's table CRUD methods.
    /// </summary>
    public class FleetRepository : IFleetRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreatePlane(FLEET plane)
        {
            this.db.FLEET.Add(plane);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeletePlane(string id)
        {
            this.db.FLEET.Remove(this.db.FLEET.First(x => x.FLEET_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<FLEET> GetAll()
        {
            return this.db.FLEET;
        }

        /// <inheritdoc />
        public FLEET GetOne(string id)
        {
            return this.db.FLEET.First(x => x.FLEET_ID == id);
        }

        /// <inheritdoc />
        public void UpdateCallsign(string id, string callsign)
        {
            this.db.FLEET.First(x => x.FLEET_ID == id).CALLSIGN = callsign;
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void UpdateCapacity(string id, short capacity)
        {
            this.db.FLEET.First(x => x.FLEET_ID == id).CAPACITY = capacity;
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void UpdateStatus(string id, string status)
        {
            this.db.FLEET.First(x => x.FLEET_ID == id).STATUS = status;
            this.db.SaveChanges();
        }
    }
}
