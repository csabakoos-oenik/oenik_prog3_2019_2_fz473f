﻿// <copyright file="IFlightAttendantsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The FLIGHT_ATTENDANTS's CRUD operations.
    /// </summary>
    public interface IFlightAttendantsRepository : IRepository<FLIGHT_ATTENDANTS, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a employee and adds it to the FLIGHT_ATTENDANTS table.
        /// </summary>
        /// <param name="employee">The new employee.</param>
        void CreateEmployee(FLIGHT_ATTENDANTS employee);

        // update

        /// <summary>
        /// Updates the selected employee's spoken languages's number.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        /// <param name="languages">The employee's new number of languages spoken.</param>
        void UpdateLanguages(int id, short languages);

        // delete

        /// <summary>
        /// Deletes the selected employee from the FLIGHT_ATTENDANTS table.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        void DeleteEmployee(int id);
    }
}
