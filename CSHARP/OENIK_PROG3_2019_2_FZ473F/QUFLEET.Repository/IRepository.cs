﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The CRUD operations are outsourced to this interface.
    /// </summary>
    /// <typeparam name="T">The CRUD operations are used on this class.</typeparam>
    /// <typeparam name="T_ID">The ID's type.</typeparam>
    public interface IRepository<T, T_ID>
        where T : class
    {
        /// <summary>
        /// This method returns a selected record from the table.
        /// </summary>
        /// <param name="id">The ID of the required record.</param>
        /// <returns>A record of the table.</returns>
        T GetOne(T_ID id);

        /// <summary>
        /// This method returns the full content of the given T table.
        /// </summary>
        /// <returns>Full content of the table.</returns>
        IQueryable<T> GetAll();
    }
}
