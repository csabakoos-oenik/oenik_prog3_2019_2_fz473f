﻿// <copyright file="IPilotsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The PILOTS's CRUD operations.
    /// </summary>
    public interface IPilotsRepository : IRepository<PILOT, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a new pilot and adds it to the PILOTS table.
        /// </summary>
        /// <param name="pilot">The new pilot.</param>
        void CreatePilot(PILOT pilot);

        // update

        /// <summary>
        /// Updates the selected pilot's flying hours.
        /// </summary>
        /// <param name="id">The pilot's id.</param>
        /// <param name="hours">The pilot's new flying hours.</param>
        void UpdateFlyingHours(int id, int hours);

        // delete

        /// <summary>
        /// Deletes selected pilot from the PILOTS table.
        /// </summary>
        /// <param name="id">The pilot's id.</param>
        void DeletePilot(int id);
    }
}
