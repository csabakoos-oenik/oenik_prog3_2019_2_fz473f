﻿// <copyright file="IEmployeesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The EMPLOYEES's CRUD operations.
    /// </summary>
    public interface IEmployeesRepository : IRepository<EMPLOYEE, int>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a new employee and adds it to the EMPLOYEES table.
        /// </summary>
        /// <param name="employee">The new employee.</param>
        void CreateEmployee(EMPLOYEE employee);

        // update

        /// <summary>
        /// Updates the selected employee's name.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        /// <param name="name">The employee's new name.</param>
        void UpdateName(int id, string name);

        /// <summary>
        /// Updates the selected employee's salary.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        /// <param name="salary">The employee's new salary.</param>
        void UpdateSalary(int id, int salary);

        // delete

        /// <summary>
        /// Deletes the selected employee from the EMPLOYEES table.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        void DeleteEmployee(int id);
    }
}
