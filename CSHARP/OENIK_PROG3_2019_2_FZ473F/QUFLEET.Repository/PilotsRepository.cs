﻿// <copyright file="PilotsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the PILOTS's table CRUD methods.
    /// </summary>
    public class PilotsRepository : IPilotsRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreatePilot(PILOT pilot)
        {
            this.db.PILOTS.Add(pilot);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeletePilot(int id)
        {
            this.db.PILOTS.Remove(this.db.PILOTS.First(x => x.EMPLOYEE_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<PILOT> GetAll()
        {
            return this.db.PILOTS;
        }

        /// <inheritdoc />
        public PILOT GetOne(int id)
        {
            return this.db.PILOTS.First(x => x.EMPLOYEE_ID == id);
        }

        /// <inheritdoc />
        public void UpdateFlyingHours(int id, int hours)
        {
            this.db.PILOTS.First(x => x.EMPLOYEE_ID == id).FLYING_HOURS = hours;
            this.db.SaveChanges();
        }
    }
}
