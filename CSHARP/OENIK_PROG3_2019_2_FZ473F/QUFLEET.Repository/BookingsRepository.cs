﻿// <copyright file="BookingsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// This class contains the BOOKINGS's table CRUD methods.
    /// </summary>
    public class BookingsRepository : IBookingsRepository
    {
        private QUJETDatabaseEntities db = new QUJETDatabaseEntities();

        /// <inheritdoc />
        public void CreateBooking(BOOKING booking)
        {
            this.db.BOOKINGS.Add(booking);
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public void DeleteBooking(int id)
        {
            this.db.BOOKINGS.Remove(this.db.BOOKINGS.First(x => x.BOOKING_ID == id));
            this.db.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<BOOKING> GetAll()
        {
            return this.db.BOOKINGS;
        }

        /// <inheritdoc />
        public BOOKING GetOne(int id)
        {
            return this.db.BOOKINGS.First(x => x.BOOKING_ID == id);
        }
    }
}
