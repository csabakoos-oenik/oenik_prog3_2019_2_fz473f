﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Program
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;
    using QUFLEET.Logic;

    /// <summary>
    /// This static class is the navigation menu for the program.
    /// </summary>
    public static class Menu
    {
        private static AirportsLogic airportsLogic = new AirportsLogic();
        private static FleetLogic fleetLogic = new FleetLogic();
        private static FlightsLogic flightLogic = new FlightsLogic();
        private static EmployeesLogic employeesLogic = new EmployeesLogic();
        private static BookingsLogic bookingsLogic = new BookingsLogic();

        /// <summary>
        /// This static methods is initialising the menu.
        /// </summary>
        public static void Initialise()
        {
            int selectedOption = 1;
            while (selectedOption != 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("#SYSLOG: Program started.");
                Console.ForegroundColor = ConsoleColor.Gray;
            GoHome:
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------------");
                Console.WriteLine(" Main options");
                Console.WriteLine("--------------------------------------------------");
                Console.WriteLine();
                Console.WriteLine("1) List items from the database");
                Console.WriteLine("2) Add items to the database");
                Console.WriteLine("3) Modify items in the database");
                Console.WriteLine("4) Delete items from the database");
                Console.WriteLine("5) Get the details of a plane");
                Console.WriteLine("6) Summarized reports on available airports");
                Console.WriteLine("7) Abstract details of current employees");
                Console.WriteLine("8) Complete statement of a booking");
                Console.WriteLine("9) Book a flight");
                Console.WriteLine("0) Exit");
                Console.WriteLine();
                Console.Write("Enter a number to select an option: ");

                try
                {
                    selectedOption = int.Parse(Console.ReadLine());

                    switch (selectedOption)
                    {
                        case 0:
                            break;
                        case 1:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"List items from the database\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        GoList:
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" List items from the database ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.WriteLine("1) Destination airports");
                            Console.WriteLine("2) Current fleet");
                            Console.WriteLine("3) Employees");
                            Console.WriteLine("4) Available flight routes");
                            Console.WriteLine("5) Bookings");
                            Console.WriteLine("0) Back");
                            Console.WriteLine();
                            Console.Write("Enter a number to select an option: ");

                            try
                            {
                                selectedOption = int.Parse(Console.ReadLine());

                                switch (selectedOption)
                                {
                                    case 0:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoHome;
                                    case 1:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Destination airports\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" List of Destination airports ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("ID\tIATA\tICAO\tFAA\tCOUTRY\tCITY");
                                        var airports = airportsLogic.GetAll();
                                        foreach (var item in airports)
                                        {
                                            Console.WriteLine(item.AIRPORT_ID + "\t" + item.IATA + "\t" + item.ICAO + "\t" + item.FAA + "\t" + item.COUNTRY + "\t" + item.CITY);
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"List items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoList;
                                    case 2:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Current fleet\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" List of Current fleet ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("ID\tCALLSIGN\tCAPACITY\tMODEL\t\t\t\tSTATUS");
                                        var fleet = fleetLogic.GetAll();
                                        foreach (var item in fleet)
                                        {
                                            Console.WriteLine(item.FLEET_ID + "\t" + item.CALLSIGN + "\t\t" + item.CAPACITY + "\t\t" + item.MODEL + "\t\t\t" + item.STATUS);
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"List items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoList;
                                    case 3:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Employees\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" List of Employees ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("ID\tNAME\tSALARY\tHIRE_DATE");
                                        var employees = employeesLogic.GetAll();
                                        foreach (var item in employees)
                                        {
                                            Console.WriteLine($"{item.EMPLOYEE_ID}\t{item.NAME}\t{item.SALARY}\t{item.HIRE_DATE}");
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"List items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoList;
                                    case 4:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Available flight routes\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" List of Available flight routes ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("ID\tPLANE\tDEPARTURE\tDEPARTURE_DATE\tARRIVAL\tARRIVAL_DATE\t\tCLASS\tPRICE");
                                        var flights = flightLogic.GetAll();
                                        foreach (var item in flights)
                                        {
                                            Console.WriteLine($"{item.FLIGHT_ID}\t{item.FLEET_ID}\t{item.DEPARTURE}\t{item.DEPART_DATE}\t{item.ARRIVAL}\t{item.ARRIVE_DATE}\t{item.CLASS}\t{item.PRICE}");
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"List items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoList;
                                    case 5:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Bookings\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" List of Bookings ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("ID\tFLIGHT\tNAME");
                                        var bookings = bookingsLogic.GetAll();
                                        foreach (var item in bookings)
                                        {
                                            Console.WriteLine($"{item.BOOKING_ID}\t{item.FLIGHT_ID}\t{item.NAME}");
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"List items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoList;
                                    default:
                                        throw new NotExistingOptionException();
                                }
                            }
                            catch (NotExistingOptionException e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoList;
                            }
                            catch (FormatException)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: Given option is inadequate! Please enter a valid option!");
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoList;
                            }

                        case 2:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Add items to the database\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        GoAdd:
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Add items to the database ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.WriteLine("1) Airport");
                            Console.WriteLine("2) Plane");
                            Console.WriteLine("3) Employee");
                            Console.WriteLine("0) Back");
                            Console.WriteLine();
                            Console.Write("Enter a number to select an option: ");

                            try
                            {
                                selectedOption = int.Parse(Console.ReadLine());

                                switch (selectedOption)
                                {
                                    case 0:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoHome;
                                    case 1:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Airport\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Add given Airport ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("Enter the new airport's properties below!");
                                        Console.Write("> IATA: ");
                                        string iata = Console.ReadLine();
                                        Console.Write("> ICAO: ");
                                        string icao = Console.ReadLine();
                                        Console.Write("> FAA: ");
                                        string faa = Console.ReadLine();
                                        Console.Write("> Country: ");
                                        string country = Console.ReadLine();
                                        Console.Write("> City: ");
                                        string city = Console.ReadLine();
                                        try
                                        {
                                            airportsLogic.CreateAirport(iata, icao, faa, country, city);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoAdd;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Add items to the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoAdd;
                                    case 2:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Plane\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Add given Plane ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("Enter the new plane's properties below!");
                                        Console.Write("> Callsign: ");
                                        string callsign = Console.ReadLine();
                                        Console.Write("> Capacity: ");
                                        string capacity = Console.ReadLine();
                                        Console.Write("> Status: ");
                                        string status = Console.ReadLine();
                                        Console.Write("> Model: ");
                                        string model = Console.ReadLine();
                                        try
                                        {
                                            fleetLogic.CreatePlane(callsign, int.Parse(capacity), model, status);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoAdd;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Add items to the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoAdd;
                                    case 3:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Employee\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Add give Employee ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.WriteLine("Enter the new employee's properties below!");
                                        Console.Write("> Name: ");
                                        string name = Console.ReadLine();
                                        Console.Write("> Salary: ");
                                        string salary = Console.ReadLine();
                                        try
                                        {
                                            employeesLogic.CreateEmployee(name, int.Parse(salary));
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoAdd;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Add items to the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoAdd;
                                    default:
                                        throw new NotExistingOptionException();
                                }
                            }
                            catch (NotExistingOptionException e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoAdd;
                            }
                            catch (FormatException)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: Given option is inadequate! Please enter a valid option!");
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoAdd;
                            }

                        case 3:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Modify items in the database\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        GoModify:
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Modify items in the database ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.WriteLine("1) Airport's IATA");
                            Console.WriteLine("2) Airport's ICAO");
                            Console.WriteLine("3) Airport's FAA");
                            Console.WriteLine("4) Plane's capacity");
                            Console.WriteLine("5) Plane's status");
                            Console.WriteLine("6) Employee's salary");
                            Console.WriteLine("0) Back");
                            Console.WriteLine();
                            Console.Write("Enter a number to select an option: ");

                            try
                            {
                                selectedOption = int.Parse(Console.ReadLine());

                                switch (selectedOption)
                                {
                                    case 0:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoHome;
                                    case 1:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Airport's IATA\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Modify given Airport's IATA ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to modify: ");
                                        int airportID = int.Parse(Console.ReadLine());
                                        Console.Write("> Enter the new IATA value: ");
                                        string iata = Console.ReadLine();
                                        try
                                        {
                                            airportsLogic.UpdateIATA(airportID, iata);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoModify;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Modify items in the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoModify;
                                    case 2:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Airport's ICAO\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Modify given Airport's ICAO ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to modify: ");
                                        airportID = int.Parse(Console.ReadLine());
                                        Console.Write("> Enter the new ICAO value: ");
                                        string icao = Console.ReadLine();
                                        try
                                        {
                                            airportsLogic.UpdateICAO(airportID, icao);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoModify;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Modify items in the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoModify;
                                    case 3:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Airport's FAA\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Modify given Airport's FAA ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to modify: ");
                                        airportID = int.Parse(Console.ReadLine());
                                        Console.Write("> Enter the new FAA value: ");
                                        string faa = Console.ReadLine();
                                        try
                                        {
                                            airportsLogic.UpdateFAA(airportID, faa);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoModify;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Modify items in the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoModify;
                                    case 4:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Plane's capacity\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Modify given Plane's capacity ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the plane to modify: ");
                                        string fleetID = Console.ReadLine();
                                        Console.Write("> Enter the new capacity value: ");
                                        short capacity = short.Parse(Console.ReadLine());
                                        try
                                        {
                                            fleetLogic.UpdateCapacity(fleetID, capacity);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoModify;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Modify items in the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoModify;
                                    case 5:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Plane's capacity\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Modify given Plane's status ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the plane to modify: ");
                                        fleetID = Console.ReadLine();
                                        Console.Write("> Enter the new status value: ");
                                        string status = Console.ReadLine();
                                        try
                                        {
                                            fleetLogic.UpdateStatus(fleetID, status);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoModify;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Modify items in the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoModify;
                                    case 6:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Employee's salary\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Modify given Employee's salary ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the employee to modify: ");
                                        int employeeID = int.Parse(Console.ReadLine());
                                        Console.Write("> Enter the new salary value: ");
                                        int salary = int.Parse(Console.ReadLine());
                                        try
                                        {
                                            employeesLogic.UpdateSalary(employeeID, salary);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoModify;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Modify items in the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoModify;
                                    default:
                                        throw new NotExistingOptionException();
                                }
                            }
                            catch (NotExistingOptionException e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoModify;
                            }
                            catch (FormatException)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: Given option is inadequate! Please enter a valid option!");
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoModify;
                            }

                        case 4:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Delete items from the database\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        GoDelete:
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Delete items from the database ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.WriteLine("1) Airport");
                            Console.WriteLine("2) Plane");
                            Console.WriteLine("3) Employee");
                            Console.WriteLine("4) Flight route");
                            Console.WriteLine("5) Booking");
                            Console.WriteLine("0) Back");
                            Console.WriteLine();
                            Console.Write("Enter a number to select an option: ");

                            try
                            {
                                selectedOption = int.Parse(Console.ReadLine());

                                switch (selectedOption)
                                {
                                    case 0:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoHome;
                                    case 1:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Airport\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Delete given Airport ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to delete: ");
                                        int airportID = int.Parse(Console.ReadLine());
                                        try
                                        {
                                            airportsLogic.DeleteAirport(airportID);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoDelete;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Delete items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoDelete;
                                    case 2:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Plane\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Delete given Plane ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to delete: ");
                                        string fleetID = Console.ReadLine();
                                        try
                                        {
                                            fleetLogic.DeletePlane(fleetID);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoDelete;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Delete items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoDelete;
                                    case 3:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Employee\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Delete given Employee ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to delete: ");
                                        int employeeID = int.Parse(Console.ReadLine());
                                        try
                                        {
                                            employeesLogic.DeleteEmployee(employeeID);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoDelete;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Delete items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoDelete;
                                    case 4:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Flight route\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Delete given Flight route ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to delete: ");
                                        int flightID = int.Parse(Console.ReadLine());
                                        try
                                        {
                                            flightLogic.DeleteFlight(flightID);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoDelete;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Delete items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoDelete;
                                    case 5:
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Selected \"Booking\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.WriteLine();
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine(" Delete given Booking ");
                                        Console.WriteLine("--------------------------------------------------");
                                        Console.WriteLine();
                                        Console.Write("> Enter the ID of the airport to delete: ");
                                        int bookingID = int.Parse(Console.ReadLine());
                                        try
                                        {
                                            bookingsLogic.DeleteBooking(bookingID);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            Console.WriteLine("#SYSLOG: " + e.Message.Split('.')[0] + ".");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            goto GoDelete;
                                        }

                                        Console.WriteLine();
                                        Console.WriteLine("Press a button to go back...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.WriteLine("#SYSLOG: Returned to \"Delete items from the database\".");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        goto GoDelete;
                                    default:
                                        throw new NotExistingOptionException();
                                }
                            }
                            catch (NotExistingOptionException e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoDelete;
                            }
                            catch (FormatException)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: Given option is inadequate! Please enter a valid option!");
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoDelete;
                            }

                        case 5:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Get the details of a plane\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Get the details of a plane ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.Write("Enter the plane's id: ");
                            try
                            {
                                string id = Console.ReadLine();
                                List<string> details = fleetLogic.PlaneDetails(id);

                                Console.WriteLine();

                                foreach (string detail in details)
                                {
                                    Console.WriteLine(detail);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoHome;
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press a button to go back...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            goto GoHome;

                        case 6:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Summarized reports on available airports\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Summarized reports on available airports ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            try
                            {
                                List<string> reports = airportsLogic.SummarizedReport();

                                foreach (string report in reports)
                                {
                                    Console.WriteLine(report);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoHome;
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press a button to go back...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            goto GoHome;

                        case 7:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Abstract details of current employees\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Abstract details of current employees ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            try
                            {
                                List<string> details = employeesLogic.AbstractDetails();

                                foreach (string detail in details)
                                {
                                    Console.WriteLine(detail);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoHome;
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press a button to go back...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            goto GoHome;

                        case 8:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Complete statement of a booking\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Complete statement of a booking ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.Write("Enter the booking's id: ");
                            try
                            {
                                int id = int.Parse(Console.ReadLine());
                                List<string> statement = bookingsLogic.CompleteStatement(id);

                                Console.WriteLine();

                                foreach (string line in statement)
                                {
                                    Console.WriteLine(line);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoHome;
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press a button to go back...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            goto GoHome;

                        case 9:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Selected \"Book a flight\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine();
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine(" Book a flight ");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine();
                            Console.WriteLine("Press ENTER to be redirected to the booking page...");
                            Console.ReadLine();
                            try
                            {
                                Process.Start("http://localhost:8080/OENIK_PROG3_2019_2_FZ473F/");
                                Console.WriteLine("> Waiting for the booking...");
                                System.Threading.Thread.Sleep(500);
                                Console.WriteLine("> Listening on port 1034...");
                                bookingsLogic.RecieveBooking();
                                Console.WriteLine("> The booking has been recieved...");
                                bookingsLogic.Book();
                                Console.WriteLine("> The booking has created in the database.");
                            }
                            catch (Exception e)
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("#SYSLOG: " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                goto GoHome;
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press a button to go back...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("#SYSLOG: Returned to \"Main options\".");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            goto GoHome;
                        default:
                            throw new NotExistingOptionException();
                    }
                }
                catch (NotExistingOptionException e)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("#SYSLOG: " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    goto GoHome;
                }
                catch (FormatException)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("#SYSLOG: Given option is inadequate! Please enter a valid option!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    goto GoHome;
                }
            }
        }
    }
}
