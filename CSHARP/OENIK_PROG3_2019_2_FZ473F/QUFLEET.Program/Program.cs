﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Program starts here.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// This is where the program launches.
        /// </summary>
        private static void Main()
        {
            Menu.Initialise();
        }
    }
}
