var class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository =
[
    [ "CreatePlane", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#ad7be6b7fc0e0cb362f766ee70344a41e", null ],
    [ "DeletePlane", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#a96c136de984ba93355721426f017969c", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#a6b566f6ff77297a6f9598bf99983053b", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#a6251b557f0b8dcac155f2132bbbfa5fd", null ],
    [ "UpdateCallsign", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#a1784d88c77662aebbc015d132c20d934", null ],
    [ "UpdateCapacity", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#a83be0ba3fedb9162eb17afd51c169e0e", null ],
    [ "UpdateStatus", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html#a74a369b402d9ed415ae031979f78cb70", null ]
];