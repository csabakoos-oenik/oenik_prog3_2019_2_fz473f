var class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic =
[
    [ "FleetLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#acd6bccf757379a5580ab969cd712376e", null ],
    [ "CreatePlane", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a08487930caec9e6d13579d04573bb34b", null ],
    [ "DeletePlane", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a2dca75057198c0632903ab76f6808ae8", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a706fe1cc3766eb96c64dbb0b4763ad8f", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a77de5819319883e640d72bd65b4bb9ff", null ],
    [ "PlaneDetails", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#aa70df7d8a4f43e1b350a7a0f8aac24bd", null ],
    [ "UpdateCallsign", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a94baaf804180673a342cae3ee2091547", null ],
    [ "UpdateCapacity", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a096c3048b960a357d66bfb469517b3e8", null ],
    [ "UpdateStatus", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html#a31407155d47852ac0505a5f6ebc31737", null ]
];