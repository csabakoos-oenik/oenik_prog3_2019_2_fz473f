var interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository =
[
    [ "CreateEmployee", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository.html#a6c8ef1386e6214f94fa0e7ac426b0553", null ],
    [ "DeleteEmployee", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository.html#ab7a67e72261c06928516382c5423766d", null ],
    [ "UpdateName", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository.html#a58df9737125774c0f8d5bd9bc7b94f34", null ],
    [ "UpdateSalary", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository.html#adfab62cee65b2da359d669939485ed18", null ]
];