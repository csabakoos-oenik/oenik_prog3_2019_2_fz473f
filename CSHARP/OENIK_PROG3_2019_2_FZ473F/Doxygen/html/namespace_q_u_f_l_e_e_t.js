var namespace_q_u_f_l_e_e_t =
[
    [ "Data", "namespace_q_u_f_l_e_e_t_1_1_data.html", "namespace_q_u_f_l_e_e_t_1_1_data" ],
    [ "Logic", "namespace_q_u_f_l_e_e_t_1_1_logic.html", "namespace_q_u_f_l_e_e_t_1_1_logic" ],
    [ "Program", "namespace_q_u_f_l_e_e_t_1_1_program.html", "namespace_q_u_f_l_e_e_t_1_1_program" ],
    [ "Repository", "namespace_q_u_f_l_e_e_t_1_1_repository.html", "namespace_q_u_f_l_e_e_t_1_1_repository" ]
];