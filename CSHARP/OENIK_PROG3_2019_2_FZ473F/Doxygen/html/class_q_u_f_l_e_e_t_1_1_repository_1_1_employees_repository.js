var class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository =
[
    [ "CreateEmployee", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html#a1eb7a35a3cf6d0201020779b09bed6f7", null ],
    [ "DeleteEmployee", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html#a07ade31b5e9d1d729510d01674cc618a", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html#ac4363aa82defe00fbd853f147defdd0d", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html#a61e5c99091124e4f2f2e517445d5547e", null ],
    [ "UpdateName", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html#ac7efed2ef54ae1785188d0a7d36b6de6", null ],
    [ "UpdateSalary", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html#a160f5048ea241bb394854574184b6c05", null ]
];