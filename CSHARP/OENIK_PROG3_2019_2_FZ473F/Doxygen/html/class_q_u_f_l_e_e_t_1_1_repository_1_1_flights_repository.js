var class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository =
[
    [ "CreateFlight", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html#a7d540b82d457c0124d9428ce34178e65", null ],
    [ "DeleteFlight", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html#a9e9a7b599b4103a6dfe752873f33a3d7", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html#a94f665ca63dbcd0e838a3947e3a29325", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html#ad7fbab9505b62385756d5c6fff2a482c", null ],
    [ "UpdatePrice", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html#a0ae050cb3cdaaf683ce9b93e05da1af5", null ]
];