var interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository =
[
    [ "CreateAirport", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html#ac21b06c9d07ef278a2f2f9fd5e96ba07", null ],
    [ "DeleteAirport", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html#aa6fa6e43ae3dc2fd3d23da1fa51f7a24", null ],
    [ "UpdateFAA", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html#a60c740d89814447be9069ecb795be9f9", null ],
    [ "UpdateIATA", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html#a0d5e4ec7666fe6c0b4bb7236f7c8bb5a", null ],
    [ "UpdateICAO", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html#a1cf34e9900f7738976b2c9f5c50a57de", null ]
];