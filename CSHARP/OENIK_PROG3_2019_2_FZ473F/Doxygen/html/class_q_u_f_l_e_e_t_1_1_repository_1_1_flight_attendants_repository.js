var class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository =
[
    [ "CreateEmployee", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html#a3a24ac5c762a43fa77008379d6a2a1a6", null ],
    [ "DeleteEmployee", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html#a43baeffc33fd61ee10bac541c4155968", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html#a9b28079a1820b16293a6cc2317962d65", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html#a26001813037c08a9837b662beb4bda77", null ],
    [ "UpdateLanguages", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html#a7355a9709b49f940730d5b57e1dc71e7", null ]
];