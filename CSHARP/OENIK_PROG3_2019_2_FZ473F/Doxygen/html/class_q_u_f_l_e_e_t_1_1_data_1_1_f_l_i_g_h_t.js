var class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t =
[
    [ "FLIGHT", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a7eff77b2372f3d149050dd9001dbbce3", null ],
    [ "AIRPORT", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a1a1bccd150a4eaabea75718069cc3e67", null ],
    [ "AIRPORT1", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#ab81ad49cdec76f6f3a8b5c9ab0f62424", null ],
    [ "ARRIVAL", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a5173528ef70960d34c744a69d7afcbb9", null ],
    [ "ARRIVE_DATE", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a0b12c4e7b8550be46e348c39b5dd17de", null ],
    [ "BOOKINGS", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a612faa3539bea3544b28ecdca264bd59", null ],
    [ "CLASS", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a49c76b6aec1a932e35cb5c7794e8f134", null ],
    [ "DEPART_DATE", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a9a2c53711336085a41ec341f7e92b1fe", null ],
    [ "DEPARTURE", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#ac764b3ab7e2f5e46fedffa38e2f805b9", null ],
    [ "FLEET", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a93424df99fb403b1de777000aae47fbd", null ],
    [ "FLEET_ID", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a3d3270cf7c096f87cfed0f6e6ad0bfcb", null ],
    [ "FLIGHT_ID", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a8f658a225a5fb1d3e8623f94a0d56bd7", null ],
    [ "PRICE", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html#a418c4c9b1e55c36d8fc4e8898a1c62ac", null ]
];