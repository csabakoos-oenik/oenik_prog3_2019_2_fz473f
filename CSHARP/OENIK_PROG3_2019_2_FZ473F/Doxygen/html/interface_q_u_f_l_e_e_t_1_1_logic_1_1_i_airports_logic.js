var interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic =
[
    [ "CreateAirport", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html#a9bf9170a457dc0ef6b00444e4834d9e4", null ],
    [ "DeleteAirport", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html#a2a04aee9157809a57a9d1d8a33faf69b", null ],
    [ "UpdateFAA", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html#a74c9fd89dff948f447fb61406eb35a36", null ],
    [ "UpdateIATA", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html#a4a62f9270601c2fcaf35233c8a4d4122", null ],
    [ "UpdateICAO", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html#a9e715b3adc65117c70fcdc35d60c83bf", null ]
];