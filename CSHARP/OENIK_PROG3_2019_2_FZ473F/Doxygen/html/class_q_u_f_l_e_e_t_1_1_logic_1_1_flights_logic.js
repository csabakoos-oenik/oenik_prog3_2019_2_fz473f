var class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic =
[
    [ "FlightsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#a0dcee460e6a311c5ad2f79ab576d0618", null ],
    [ "CreateFlight", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#a65077723601daa68a03b61c9db8d972d", null ],
    [ "DeleteFlight", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#a58e31c128d2936bd43c4ba34275b3918", null ],
    [ "FlightsToXML", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#a594ff292b4fa6f734b68f112733c766a", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#abc2bddef4b080bc453062df3a4fdf2ea", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#a540d19680b0f131e4d0a4b6cd8eff96b", null ],
    [ "UpdatePrice", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html#aa7621c6a7cad2f04203eccbef28ebb6a", null ]
];