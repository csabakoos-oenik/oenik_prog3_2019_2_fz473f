var class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic =
[
    [ "AirportsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#a5c07a4b8ae1f51720fe03b28a14fbb8c", null ],
    [ "CreateAirport", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#ab3f88e69167490002a339329bd30a4e6", null ],
    [ "DeleteAirport", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#a9c850049c37882305e635bd202007b69", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#a8b28fdec2750b7c182c143b1816363d7", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#abfff54cc3a53307fa999eec89d3ef177", null ],
    [ "SummarizedReport", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#a343548c87da13eb97c359d29b34e0922", null ],
    [ "UpdateFAA", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#a9c94a16dec99ec2c2b25389f19bdc841", null ],
    [ "UpdateIATA", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#aedd63295b96334d44c12794041a68fe3", null ],
    [ "UpdateICAO", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html#a55afa050451b2b92f53384bbfff99e94", null ]
];