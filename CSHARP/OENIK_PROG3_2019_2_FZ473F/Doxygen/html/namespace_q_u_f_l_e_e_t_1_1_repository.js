var namespace_q_u_f_l_e_e_t_1_1_repository =
[
    [ "AirportsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository" ],
    [ "BookingsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_bookings_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_bookings_repository" ],
    [ "EmployeesRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository" ],
    [ "FleetRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository" ],
    [ "FlightAttendantsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository" ],
    [ "FlightsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository" ],
    [ "IAirportsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository" ],
    [ "IBookingsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_bookings_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_bookings_repository" ],
    [ "IEmployeesRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository" ],
    [ "IFleetRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository" ],
    [ "IFlightAttendantsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_flight_attendants_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_flight_attendants_repository" ],
    [ "IFlightsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_flights_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_flights_repository" ],
    [ "IMaintenanceCrewRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_maintenance_crew_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_maintenance_crew_repository" ],
    [ "IPilotsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_pilots_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_pilots_repository" ],
    [ "IRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository" ],
    [ "MaintenanceCrewRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_maintenance_crew_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_maintenance_crew_repository" ],
    [ "PilotsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository" ]
];