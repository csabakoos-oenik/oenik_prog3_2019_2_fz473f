var class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests =
[
    [ "CreateEmployeeTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#a563c4f217f60b7d23188fbe128e4488c", null ],
    [ "DeleteEmployeeTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#a84915960451765d223a40b63ea910e7f", null ],
    [ "GetAllTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#a71b6beea1db8ee90238c933639c64d9f", null ],
    [ "GetOneTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#a4a7b9e5448d3e193d36fd68cdb2db763", null ],
    [ "Init", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#ac55c22f6af7a309bd9cf4e6b8766acc5", null ],
    [ "UpdateNameTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#ab64e73bee300c9a4e0b98ec623a016eb", null ],
    [ "UpdateSalaryTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html#a8781563a585ec0357edb340bcb9e6e6d", null ]
];