var class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests =
[
    [ "DeletePlaneTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#afdcac42d0721e89787368fa4ffb49b39", null ],
    [ "GetAllTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#ac94592045060c15d421bcbde4895aa7d", null ],
    [ "GetOneTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#aaa5caaab69ca4948901a52670097e62a", null ],
    [ "Init", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#ab0a4896b1c4e5571950d613711ed2b31", null ],
    [ "UpdateCallsignTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#a88545889d3bc5a3e663be58f35d4dacd", null ],
    [ "UpdateCapacityTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#a2db6699d4280d45ba7be18f235681349", null ],
    [ "UpdateStatusTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html#a7dc3a616175ae6a0f09003df6bf10a01", null ]
];