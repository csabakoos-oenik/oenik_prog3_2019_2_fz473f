var namespace_q_u_f_l_e_e_t_1_1_logic =
[
    [ "Tests", "namespace_q_u_f_l_e_e_t_1_1_logic_1_1_tests.html", "namespace_q_u_f_l_e_e_t_1_1_logic_1_1_tests" ],
    [ "AirportsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic" ],
    [ "BookingsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_bookings_logic.html", "class_q_u_f_l_e_e_t_1_1_logic_1_1_bookings_logic" ],
    [ "EmployeesLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic" ],
    [ "FleetLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic" ],
    [ "FlightsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic" ],
    [ "IAirportsLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic" ],
    [ "IBookingsLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_bookings_logic.html", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_bookings_logic" ],
    [ "IEmployeesLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_employees_logic.html", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_employees_logic" ],
    [ "IFleetLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic" ],
    [ "IFlightsLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_flights_logic.html", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_flights_logic" ],
    [ "ILogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic" ]
];