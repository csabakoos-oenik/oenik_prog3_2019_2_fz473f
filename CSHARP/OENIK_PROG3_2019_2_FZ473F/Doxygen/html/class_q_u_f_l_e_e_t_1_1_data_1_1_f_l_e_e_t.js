var class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t =
[
    [ "FLEET", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#af78f561e49466d66d6ec94560dd5160a", null ],
    [ "CALLSIGN", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#af257cae7266bced88b27b64cb8a73db7", null ],
    [ "CAPACITY", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#a77228907c13bb9d1966d130e09fd4d2b", null ],
    [ "EMPLOYEES", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#afc83b6d65b514b677ccf12c39943e2cf", null ],
    [ "FLEET_ID", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#a9ca03952ca99b6a05ac0fcb9456207fb", null ],
    [ "FLIGHTS", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#a7386e80ea30aa3e523c2989743206cbf", null ],
    [ "MODEL", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#a3e18e51aa42365ab747a228c1048f274", null ],
    [ "STATUS", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html#ae0ff25bcde92786fd0767588eb539bb2", null ]
];