var class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic =
[
    [ "EmployeesLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#af433695f737182a48d54e779a3e4445c", null ],
    [ "AbstractDetails", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#a87e76da699df886dc8f4798ade285460", null ],
    [ "CreateEmployee", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#a35a4196f436f965042277e32d5b013c2", null ],
    [ "DeleteEmployee", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#a3144bd873d866d1981aba9ceb98c7e1e", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#a771cf01fbe76f78c1ddf803aa200f8ae", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#abdcd2e7bf04941c086b3a489db4af3b0", null ],
    [ "UpdateName", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#a1bea34da1b58a53775ddc8ef22f94723", null ],
    [ "UpdateSalary", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html#afd6da7af8a3238342ab0b9ec447cd426", null ]
];