var class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository =
[
    [ "CreateAirport", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#a7c9ca28358b9442db5f9c1b411e143cf", null ],
    [ "DeleteAirport", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#aecb46bf55086fb839226cdf3b51aa2c6", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#affecd561072bed5d7260d55cc541b0a6", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#aa336c820cf0aa86193a134d1d50e0e65", null ],
    [ "UpdateFAA", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#a27fcd52b23ac1475f155726c481c8243", null ],
    [ "UpdateIATA", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#a65cd23767bc2c278677da28dd2380680", null ],
    [ "UpdateICAO", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html#abdd5f423cb09024340f57d3d948fc1cd", null ]
];