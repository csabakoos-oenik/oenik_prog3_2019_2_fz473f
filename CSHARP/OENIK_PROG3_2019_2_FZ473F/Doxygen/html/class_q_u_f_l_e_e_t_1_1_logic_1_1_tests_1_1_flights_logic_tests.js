var class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests =
[
    [ "CreateFlightTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html#acc95152e14787e5cf882833b32c69e0d", null ],
    [ "DeleteFlightTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html#ad42505e1d82c8172f81a98b64ed4eb81", null ],
    [ "GetAllTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html#a849cd9796fc5927addfd796b3a9a1265", null ],
    [ "GetOneTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html#adee2afb49a89bdafd703500a4053fb41", null ],
    [ "Init", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html#a76c0bf8cc15045c7c37d637d8b684750", null ],
    [ "UpdatePriceTest", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html#a729deffe8cc16745e1c7493d931ac1bf", null ]
];