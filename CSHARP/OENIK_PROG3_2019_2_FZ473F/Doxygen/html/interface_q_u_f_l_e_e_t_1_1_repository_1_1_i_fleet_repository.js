var interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository =
[
    [ "CreatePlane", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html#a3c1844786d8b738ccae7d07b3350e454", null ],
    [ "DeletePlane", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html#a15457f26a7ca0c4880572f94804e56a3", null ],
    [ "UpdateCallsign", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html#a7f839fcf42cd985f48a6973d3ad830cc", null ],
    [ "UpdateCapacity", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html#a5cfae4d84b64e82c5eae2a99d83ce229", null ],
    [ "UpdateStatus", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html#a38ac13fca4d4fdd159df98a9cc8c97c3", null ]
];