var class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository =
[
    [ "CreatePilot", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html#ad4ce304c624b51b158d15208fdaf1c4b", null ],
    [ "DeletePilot", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html#a07fb5cab5ea1a4bed2253b0f09d36898", null ],
    [ "GetAll", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html#a9838381f655531ec1d828658821fc581", null ],
    [ "GetOne", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html#ad60c145a693075159b3586c9160e32f8", null ],
    [ "UpdateFlyingHours", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html#aa0babeb1c068fc81aecdbd9d2e35360d", null ]
];