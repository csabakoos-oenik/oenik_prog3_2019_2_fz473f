var interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic =
[
    [ "CreatePlane", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html#a80c896926b8b3cf918a11a730bdf2118", null ],
    [ "DeletePlane", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html#ae443c04193bfbdff8954033117c624d3", null ],
    [ "UpdateCallsign", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html#a84cd7f3803b56446283b7e534b87d10e", null ],
    [ "UpdateCapacity", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html#ab47e55300a0823f23be21e7b0b1dced1", null ],
    [ "UpdateStatus", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html#aeba2bba3a8ec0779869cf1b204fc4c78", null ]
];