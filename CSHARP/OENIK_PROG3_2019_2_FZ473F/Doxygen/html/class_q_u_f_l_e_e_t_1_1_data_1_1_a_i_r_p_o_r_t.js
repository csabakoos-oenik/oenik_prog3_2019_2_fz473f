var class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t =
[
    [ "AIRPORT", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a4067940cc4baf6ac2177c0f7ecb03218", null ],
    [ "AIRPORT_ID", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a54f117a66429ede8e09b8316782707ea", null ],
    [ "CITY", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a476e3080c823f2ea3a6dbcbf605d1a69", null ],
    [ "COUNTRY", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a4d98d5a952340ba932eb4b8727963807", null ],
    [ "FAA", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a2bad3dcde619d719010497262e7a686b", null ],
    [ "FLIGHTS", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#aa6391543ba2f6fabb7ab77e2ed36c009", null ],
    [ "FLIGHTS1", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#aeb7a0dcefe9d1c0cac22674f05db7cfa", null ],
    [ "IATA", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a51c66f033cad8609e9d5d93368b61884", null ],
    [ "ICAO", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html#a12ffa9b5fa8cdd7d6367db6952217710", null ]
];