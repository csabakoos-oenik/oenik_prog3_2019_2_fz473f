var namespace_q_u_f_l_e_e_t_1_1_data =
[
    [ "AIRPORT", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t" ],
    [ "BOOKING", "class_q_u_f_l_e_e_t_1_1_data_1_1_b_o_o_k_i_n_g.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_b_o_o_k_i_n_g" ],
    [ "EMPLOYEE", "class_q_u_f_l_e_e_t_1_1_data_1_1_e_m_p_l_o_y_e_e.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_e_m_p_l_o_y_e_e" ],
    [ "FLEET", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t" ],
    [ "FLIGHT", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t" ],
    [ "FLIGHT_ATTENDANTS", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t___a_t_t_e_n_d_a_n_t_s.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t___a_t_t_e_n_d_a_n_t_s" ],
    [ "MAINTENANCE_CREW", "class_q_u_f_l_e_e_t_1_1_data_1_1_m_a_i_n_t_e_n_a_n_c_e___c_r_e_w.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_m_a_i_n_t_e_n_a_n_c_e___c_r_e_w" ],
    [ "PILOT", "class_q_u_f_l_e_e_t_1_1_data_1_1_p_i_l_o_t.html", "class_q_u_f_l_e_e_t_1_1_data_1_1_p_i_l_o_t" ],
    [ "QUJETDatabaseEntities", "class_q_u_f_l_e_e_t_1_1_data_1_1_q_u_j_e_t_database_entities.html", null ]
];