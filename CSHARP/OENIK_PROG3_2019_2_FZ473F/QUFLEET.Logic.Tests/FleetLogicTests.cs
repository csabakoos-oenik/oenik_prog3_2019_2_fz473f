﻿// <copyright file="FleetLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit;
    using NUnit.Framework;
    using QUFLEET.Data;
    using QUFLEET.Logic;

    /// <summary>
    /// This class contains the tests for the FleetLogic in the Logic layer using mocked FleetRepository.
    /// </summary>
    [TestFixture]
    public class FleetLogicTests
    {
        private Mock<IFleetLogic> mockedRepository;

        /// <summary>
        /// Initialises the mocked repository.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockedRepository = new Mock<IFleetLogic>();
        }

        /// <summary>
        /// This method tests if entities can be retrieved correctly.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            List<FLEET> fleet = new List<FLEET>();
            for (int i = 1; i < 10; i++)
            {
                fleet.Add(new FLEET() { FLEET_ID = "QU-000" + i.ToString(), CALLSIGN = "OE-000" + i.ToString(), CAPACITY = 100, MODEL = "MOQ", STATUS = "Commercial" });
            }

            this.mockedRepository.Setup(x => x.GetAll()).Returns(fleet.AsQueryable<FLEET>());
            Assert.That(this.mockedRepository.Object.GetAll().Count(), Is.EqualTo(9));
        }

        /// <summary>
        /// This method tests if an instance of the table could be retrieved correctly.
        /// </summary>
        [Test]
        public void GetOneTest()
        {
            FLEET theChosenOne = new FLEET() { FLEET_ID = "QU-0001" };
            this.mockedRepository.Setup(x => x.GetOne("QU-0001")).Returns(theChosenOne);
            Assert.That(this.mockedRepository.Object.GetOne("QU-0001"), Is.EqualTo(theChosenOne));
        }

        /// <summary>
        /// This method tests if the callsign of a plane can be changed.
        /// </summary>
        [Test]
        public void UpdateCallsignTest()
        {
            this.mockedRepository.Setup(x => x.UpdateCallsign("QU-DA23", "OE-DA23")).Verifiable();
            this.mockedRepository.Object.UpdateCallsign("QU-DA23", "OE-DA23");
            this.mockedRepository.Verify(x => x.UpdateCallsign("QU-DA23", "OE-DA23"));
        }

        /// <summary>
        /// This method tests if the capacity of a plane can be changed.
        /// </summary>
        [Test]
        public void UpdateCapacityTest()
        {
            this.mockedRepository.Setup(x => x.UpdateCapacity("QU-DA23", 20)).Verifiable();
            this.mockedRepository.Object.UpdateCapacity("QU-DA23", 20);
            this.mockedRepository.Verify(x => x.UpdateCapacity("QU-DA23", 20));
        }

        /// <summary>
        /// This method tests if the status of a plane can be changed.
        /// </summary>
        [Test]
        public void UpdateStatusTest()
        {
            this.mockedRepository.Setup(x => x.UpdateStatus("QU-DA23", "Private")).Verifiable();
            this.mockedRepository.Object.UpdateStatus("QU-DA23", "Private");
            this.mockedRepository.Verify(x => x.UpdateStatus("QU-DA23", "Private"));
        }

        /// <summary>
        /// This method tests if an instance of the table can be deleted.
        /// </summary>
        [Test]
        public void DeletePlaneTest()
        {
            this.mockedRepository.Setup(x => x.DeletePlane("QU-0002")).Verifiable();
            this.mockedRepository.Object.DeletePlane("QU-0002");
            this.mockedRepository.Verify(x => x.DeletePlane("QU-0002"));
        }
    }
}
