﻿// <copyright file="AirportsLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit;
    using NUnit.Framework;
    using QUFLEET.Data;
    using QUFLEET.Logic;

    /// <summary>
    /// This class contains the tests for the AirportsLogic in the Logic layer using mocked AirportsRepository.
    /// </summary>
    [TestFixture]
    public class AirportsLogicTests
    {
        private Mock<IAirportsLogic> mockedRepository;

        /// <summary>
        /// Initialises the mocked repository and logic class.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockedRepository = new Mock<IAirportsLogic>();
        }

        /// <summary>
        /// This method tests if a new entity can be created.
        /// </summary>
        [Test]
        public void CreatePlaneTest()
        {
            this.mockedRepository.Setup(x => x.CreateAirport("XXX", "XXXX", string.Empty, "XXXXXXX", "XXXXXX")).Verifiable();
            this.mockedRepository.Object.CreateAirport("XXX", "XXXX", string.Empty, "XXXXXXX", "XXXXXX");
            this.mockedRepository.Verify(x => x.CreateAirport("XXX", "XXXX", string.Empty, "XXXXXXX", "XXXXXX"));
        }

        /// <summary>
        /// This method tests if entities can be retrieved correctly.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            List<AIRPORT> airports = new List<AIRPORT>();
            for (int i = 1; i < 100; i++)
            {
                airports.Add(new AIRPORT() { AIRPORT_ID = i, IATA = "XXX", ICAO = "XXXX", FAA = "XXX", COUNTRY = "Mock Country", CITY = "Mock City" });
            }

            this.mockedRepository.Setup(x => x.GetAll()).Returns(airports.AsQueryable<AIRPORT>());
            Assert.That(this.mockedRepository.Object.GetAll().Count(), Is.EqualTo(99));
        }

        /// <summary>
        /// This method tests if an instance of the table could be retrieved correctly.
        /// </summary>
        [Test]
        public void GetOneTest()
        {
            AIRPORT theChosenOne = new AIRPORT() { AIRPORT_ID = 97 };
            this.mockedRepository.Setup(x => x.GetOne(97)).Returns(theChosenOne);
            Assert.That(this.mockedRepository.Object.GetOne(97), Is.EqualTo(theChosenOne));
        }

        /// <summary>
        /// This method tests if the IATA of an airport can be updated.
        /// </summary>
        [Test]
        public void UpdateIATATest()
        {
            this.mockedRepository.Setup(x => x.UpdateIATA(23, "NIK")).Verifiable();
            this.mockedRepository.Object.UpdateIATA(23, "NIK");
            this.mockedRepository.Verify(x => x.UpdateIATA(23, "NIK"));
        }

        /// <summary>
        /// This method tests if the ICAO of an airport can be updated.
        /// </summary>
        [Test]
        public void UpdateICAOTest()
        {
            this.mockedRepository.Setup(x => x.UpdateICAO(45, "TEST")).Verifiable();
            this.mockedRepository.Object.UpdateICAO(45, "TEST");
            this.mockedRepository.Verify(x => x.UpdateICAO(45, "TEST"));
        }

        /// <summary>
        /// This method tests if the FAA of an airport can be updated.
        /// </summary>
        [Test]
        public void UpdateFAATest()
        {
            this.mockedRepository.Setup(x => x.UpdateFAA(523, "KVK")).Verifiable();
            this.mockedRepository.Object.UpdateFAA(523, "KVK");
            this.mockedRepository.Verify(x => x.UpdateFAA(523, "KVK"));
        }

        /// <summary>
        /// This method tests if an instance of the table can be deleted.
        /// </summary>
        [Test]
        public void DeleteAirportTest()
        {
            this.mockedRepository.Setup(x => x.DeleteAirport(1)).Verifiable();
            this.mockedRepository.Object.DeleteAirport(1);
            this.mockedRepository.Verify(x => x.DeleteAirport(1));
        }
    }
}
