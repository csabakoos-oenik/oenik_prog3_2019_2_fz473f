﻿// <copyright file="EmployeesLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit;
    using NUnit.Framework;
    using QUFLEET.Data;
    using QUFLEET.Logic;

    /// <summary>
    /// This class contains the tests for the EmployeesLogic in the Logic layer using mocked EmployeesRepository.
    /// </summary>
    [TestFixture]
    public class EmployeesLogicTests
    {
        private Mock<IEmployeesLogic> mockedRepository;

        /// <summary>
        /// Initialises the mocked repository.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockedRepository = new Mock<IEmployeesLogic>();
        }

        /// <summary>
        /// This method tests if a new employee instance could be created.
        /// </summary>
        [Test]
        public void CreateEmployeeTest()
        {
            this.mockedRepository.Setup(x => x.CreateEmployee("Moq Employee", 32000)).Verifiable();
            this.mockedRepository.Object.CreateEmployee("Moq Employee", 32000);
            this.mockedRepository.Verify(x => x.CreateEmployee("Moq Employee", 32000));
        }

        /// <summary>
        /// This method tests if every employee can be retrieved.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            List<EMPLOYEE> employees = new List<EMPLOYEE>();
            for (int i = 1; i < 20; i++)
            {
                employees.Add(new EMPLOYEE() { EMPLOYEE_ID = i, NAME = i.ToString() + ". Moq Employee", SALARY = i * 5000 });
            }

            this.mockedRepository.Setup(x => x.GetAll()).Returns(employees.AsQueryable<EMPLOYEE>());
            Assert.That(this.mockedRepository.Object.GetAll().Count(), Is.EqualTo(19));
        }

        /// <summary>
        /// This method tests if an employee can be retrieved.
        /// </summary>
        [Test]
        public void GetOneTest()
        {
            EMPLOYEE theChosenOne = new EMPLOYEE() { EMPLOYEE_ID = 5302, NAME = "Moq Employee", SALARY = 32000 };
            this.mockedRepository.Setup(x => x.GetOne(5302)).Returns(theChosenOne).Verifiable();
            this.mockedRepository.Object.GetOne(5302);
            this.mockedRepository.Verify(x => x.GetOne(5302));
        }

        /// <summary>
        /// This method tests if an employee's name can be updated.
        /// </summary>
        [Test]
        public void UpdateNameTest()
        {
            this.mockedRepository.Setup(x => x.UpdateName(43242, "New Moq Employee")).Verifiable();
            this.mockedRepository.Object.UpdateName(43242, "New Moq Employee");
            this.mockedRepository.Verify(x => x.UpdateName(43242, "New Moq Employee"));
        }

        /// <summary>
        /// This method tests if an employee's salary can be updated.
        /// </summary>
        [Test]
        public void UpdateSalaryTest()
        {
            this.mockedRepository.Setup(x => x.UpdateSalary(1034, 120000)).Verifiable();
            this.mockedRepository.Object.UpdateSalary(1034, 120000);
            this.mockedRepository.Verify(x => x.UpdateSalary(1034, 120000));
        }

        /// <summary>
        /// This method tests if a employee record can be deleted.
        /// </summary>
        [Test]
        public void DeleteEmployeeTest()
        {
            this.mockedRepository.Setup(x => x.DeleteEmployee(23543)).Verifiable();
            this.mockedRepository.Object.DeleteEmployee(23543);
            this.mockedRepository.Verify(x => x.DeleteEmployee(23543));
        }
    }
}
