﻿// <copyright file="FlightsLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit;
    using NUnit.Framework;
    using QUFLEET.Data;
    using QUFLEET.Logic;

    /// <summary>
    /// This class contains the tests for the FlightsLogic in the Logic layer using mocked FlightsRepository.
    /// </summary>
    [TestFixture]
    public class FlightsLogicTests
    {
        private Mock<IFlightsLogic> mockedRepository;

        /// <summary>
        /// Initialises the mocked repository and logic class.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockedRepository = new Mock<IFlightsLogic>();
        }

        /// <summary>
        /// This method tests if a new flight can be created.
        /// </summary>
        [Test]
        public void CreateFlightTest()
        {
            this.mockedRepository.Setup(x => x.CreateFlight(10023, 10004, "Business", 1200)).Verifiable();
            this.mockedRepository.Object.CreateFlight(10023, 10004, "Business", 1200);
            this.mockedRepository.Verify(x => x.CreateFlight(10023, 10004, "Business", 1200));
        }

        /// <summary>
        /// This method tests if every flight route can be retrieved.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            List<FLIGHT> flights = new List<FLIGHT>();
            for (int i = 60001; i <= 60100; i++)
            {
                flights.Add(new FLIGHT() { FLIGHT_ID = i });
            }

            this.mockedRepository.Setup(x => x.GetAll()).Returns(flights.AsQueryable<FLIGHT>());
            Assert.That(this.mockedRepository.Object.GetAll().Count(), Is.EqualTo(100));
        }

        /// <summary>
        /// This method tests if one flight can be retrieved correctly.
        /// </summary>
        [Test]
        public void GetOneTest()
        {
            FLIGHT theChosenOne = new FLIGHT() { FLIGHT_ID = 23131 };
            this.mockedRepository.Setup(x => x.GetOne(23131)).Returns(theChosenOne).Verifiable();
            this.mockedRepository.Object.GetOne(23131);
            this.mockedRepository.Verify(x => x.GetOne(23131));
        }

        /// <summary>
        /// This method tests if the price of a flight can be updated.
        /// </summary>
        [Test]
        public void UpdatePriceTest()
        {
            this.mockedRepository.Setup(x => x.UpdatePrice(4213, 250)).Verifiable();
            this.mockedRepository.Object.UpdatePrice(4213, 250);
            this.mockedRepository.Verify(x => x.UpdatePrice(4213, 250));
         }

        /// <summary>
        /// This method tests if a single flight can be deleted correctly.
        /// </summary>
        [Test]
        public void DeleteFlightTest()
        {
            this.mockedRepository.Setup(x => x.DeleteFlight(99)).Verifiable();
            this.mockedRepository.Object.DeleteFlight(99);
            this.mockedRepository.Verify(x => x.DeleteFlight(99));
        }
    }
}
