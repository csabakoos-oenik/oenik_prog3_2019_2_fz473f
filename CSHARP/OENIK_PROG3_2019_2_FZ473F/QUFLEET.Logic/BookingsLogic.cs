﻿// <copyright file="BookingsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;
    using QUFLEET.Repository;

    /// <summary>
    /// This class contains the FLIGHT's table CRUD methods.
    /// </summary>
    public class BookingsLogic : IBookingsLogic
    {
        private BookingsRepository bookingsRepository;
        private string bookingString;
        private int bookingFlightID;
        private string bookingName;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingsLogic"/> class.
        /// </summary>
        public BookingsLogic()
        {
            this.bookingsRepository = new BookingsRepository();
        }

        /// <inheritdoc />
        public void CreateBooking(int flight_id, string name)
        {
            BOOKING booking = new BOOKING()
            {
                BOOKING_ID = this.bookingsRepository.GetAll().OrderByDescending(x => x.BOOKING_ID).First().BOOKING_ID + 1,
                FLIGHT_ID = flight_id,
                NAME = name,
            };

            this.bookingsRepository.CreateBooking(booking);
        }

        /// <inheritdoc />
        public void DeleteBooking(int id)
        {
            this.bookingsRepository.DeleteBooking(id);
        }

        /// <inheritdoc />
        public IQueryable<BOOKING> GetAll()
        {
            return this.bookingsRepository.GetAll();
        }

        /// <inheritdoc />
        public BOOKING GetOne(int id)
        {
            return this.bookingsRepository.GetOne(id);
        }

        /// <summary>
        /// This methods recieves the booked flight int the QUFLEET Java Web Application in XML format.
        /// </summary>
        public void RecieveBooking()
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 1034);
            tcpListener.Start();

            TcpClient tcpClient = tcpListener.AcceptTcpClient();

            NetworkStream networkStream = tcpClient.GetStream();
            byte[] buffer = new byte[tcpClient.ReceiveBufferSize];

            int bytesRead = networkStream.Read(buffer, 0, tcpClient.ReceiveBufferSize);

            string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);

            tcpClient.Close();
            tcpListener.Stop();

            this.bookingString = dataReceived;
            this.bookingFlightID = int.Parse(this.bookingString.Split('|')[0]);
            this.bookingName = this.bookingString.Split('|')[1];
        }

        /// <summary>
        /// This method creates the booking that has been sent over from the Java Web Application.
        /// </summary>
        public void Book()
        {
            this.CreateBooking(this.bookingFlightID, this.bookingName);
        }

        /// <summary>
        /// This method return a complete statement of a booking.
        /// </summary>
        /// <param name="id">The booking's id.</param>
        /// <returns>The statement.</returns>
        public List<string> CompleteStatement(int id)
        {
            FlightsLogic flightsLogic = new FlightsLogic();
            AirportsLogic airportsLogic = new AirportsLogic();

            List<string> statement = new List<string>();

            BOOKING booking = this.GetOne(id);

            statement.Add($"Name:\t\t{booking.NAME}");
            statement.Add($"Departure:\t{airportsLogic.GetOne((int)flightsLogic.GetOne(booking.FLIGHT_ID).DEPARTURE).CITY} ({airportsLogic.GetOne((int)flightsLogic.GetOne(booking.FLIGHT_ID).DEPARTURE).COUNTRY})");
            statement.Add($"Arrival:\t{airportsLogic.GetOne((int)flightsLogic.GetOne(booking.FLIGHT_ID).ARRIVAL).CITY} ({airportsLogic.GetOne((int)flightsLogic.GetOne(booking.FLIGHT_ID).ARRIVAL).COUNTRY})");
            statement.Add($"Date:\t\t{flightsLogic.GetOne(booking.FLIGHT_ID).DEPART_DATE}");
            statement.Add($"Class:\t\t{flightsLogic.GetOne(booking.FLIGHT_ID).CLASS}");
            statement.Add($"Price:\t\t{flightsLogic.GetOne(booking.FLIGHT_ID).PRICE} EUR");

            return statement;
        }
    }
}
