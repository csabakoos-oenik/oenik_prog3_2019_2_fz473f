﻿// <copyright file="AirportsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;
    using QUFLEET.Repository;

    /// <summary>
    /// This class contains the AIRPORTS's table CRUD methods.
    /// </summary>
    public class AirportsLogic : IAirportsLogic
    {
        private AirportsRepository airportsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AirportsLogic"/> class.
        /// </summary>
        public AirportsLogic()
        {
            this.airportsRepository = new AirportsRepository();
        }

        /// <inheritdoc/>
        public void CreateAirport(string iata, string icao, string faa, string country, string city)
        {
            int id = this.airportsRepository.GetAll().OrderByDescending(x => x.AIRPORT_ID).First().AIRPORT_ID + 1;

            AIRPORT airport = new AIRPORT()
            {
                AIRPORT_ID = id,
                IATA = iata,
                ICAO = icao,
                FAA = faa,
                COUNTRY = country,
                CITY = city,
            };

            this.airportsRepository.CreateAirport(airport);
        }

        /// <inheritdoc/>
        public void DeleteAirport(int id)
        {
            this.airportsRepository.DeleteAirport(id);
        }

        /// <inheritdoc/>
        public AIRPORT GetOne(int id)
        {
            return this.airportsRepository.GetOne(id);
        }

        /// <inheritdoc/>
        public IQueryable<AIRPORT> GetAll()
        {
            return this.airportsRepository.GetAll();
        }

        /// <inheritdoc/>
        public void UpdateFAA(int id, string faa)
        {
            this.airportsRepository.UpdateFAA(id, faa);
        }

        /// <inheritdoc/>
        public void UpdateIATA(int id, string iata)
        {
            this.airportsRepository.UpdateIATA(id, iata);
        }

        /// <inheritdoc/>
        public void UpdateICAO(int id, string icao)
        {
            this.airportsRepository.UpdateICAO(id, icao);
        }

        /// <summary>
        /// This method returns a summarized report of the airports.
        /// </summary>
        /// <returns>The summarized report.</returns>
        public List<string> SummarizedReport()
        {
            FlightsLogic flightsLogic = new FlightsLogic();

            List<string> report = new List<string>();

            report.Add($"Number of destination cities:\t{this.GetAll().GroupBy(x => x.CITY).Count()}");
            report.Add($"Number of available countries:\t{this.GetAll().GroupBy(x => x.COUNTRY).Count()}");
            report.Add($"");
            report.Add($"Airports that has ICAO id:");

            foreach (var airport in this.GetAll().Where(x => x.ICAO != string.Empty))
            {
                report.Add($"\t{airport.AIRPORT_ID} : {airport.CITY} ({airport.COUNTRY})");
            }

            report.Add($"");
            report.Add($"Airports that has FAA id:");

            foreach (var airport in this.GetAll().Where(x => x.FAA != string.Empty))
            {
                report.Add($"\t{airport.AIRPORT_ID} : {airport.CITY} ({airport.COUNTRY})");
            }

            report.Add($"");
            report.Add($"Out of service airports:");

            foreach (var airport in this.GetAll())
            {
                int count = 0;
                foreach (var flight in flightsLogic.GetAll())
                {
                    if (flight.DEPARTURE == airport.AIRPORT_ID || flight.ARRIVAL == airport.AIRPORT_ID)
                    {
                        count++;
                    }
                }

                if (count == 0)
                {
                    report.Add($"\t{airport.AIRPORT_ID} : {airport.CITY} ({airport.COUNTRY})");
                }
            }

            return report;
        }
    }
}
