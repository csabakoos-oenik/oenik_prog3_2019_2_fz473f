﻿// <copyright file="IEmployeesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The EMPLOYEES table's CRUD operations are outsourced to this interface.
    /// </summary>
    public interface IEmployeesLogic : ILogic<EMPLOYEE, int>
    {
        /// <summary>
        /// Creates a new employee and adds it to the EMPLOYEES table.
        /// </summary>
        /// <param name="name">Employee's name.</param>
        /// <param name="salary">Employee's salary.</param>
        void CreateEmployee(string name, int salary);

        /// <summary>
        /// Updates the selected employee's name.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        /// <param name="name">The employee's new name.</param>
        void UpdateName(int id, string name);

        /// <summary>
        /// Updates the selected employee's salary.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        /// <param name="salary">The employee's new salary.</param>
        void UpdateSalary(int id, int salary);

        /// <summary>
        /// Deletes the selected employee from the EMPLOYEES table.
        /// </summary>
        /// <param name="id">The employee's id.</param>
        void DeleteEmployee(int id);
    }
}
