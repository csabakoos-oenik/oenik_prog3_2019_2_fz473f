﻿// <copyright file="EmployeesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;
    using QUFLEET.Repository;

    /// <summary>
    /// This class contains the EMPLOYEES's table CRUD methods.
    /// </summary>
    public class EmployeesLogic : IEmployeesLogic
    {
        private EmployeesRepository employeesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeesLogic"/> class.
        /// </summary>
        public EmployeesLogic()
        {
            this.employeesRepository = new EmployeesRepository();
        }

        /// <inheritdoc />
        public void CreateEmployee(string name, int salary)
        {
            EMPLOYEE employee = new EMPLOYEE()
            {
                EMPLOYEE_ID = this.employeesRepository.GetAll().OrderByDescending(x => x.EMPLOYEE_ID).First().EMPLOYEE_ID + 1,
                SALARY = salary,
                NAME = name,
                HIRE_DATE = DateTime.Now,
            };

            this.employeesRepository.CreateEmployee(employee);
        }

        /// <inheritdoc />
        public void DeleteEmployee(int id)
        {
            this.employeesRepository.DeleteEmployee(id);
        }

        /// <inheritdoc />
        public IQueryable<EMPLOYEE> GetAll()
        {
            return this.employeesRepository.GetAll();
        }

        /// <inheritdoc />
        public EMPLOYEE GetOne(int id)
        {
            return this.employeesRepository.GetOne(id);
        }

        /// <inheritdoc />
        public void UpdateName(int id, string name)
        {
            this.employeesRepository.UpdateName(id, name);
        }

        /// <inheritdoc />
        public void UpdateSalary(int id, int salary)
        {
            this.employeesRepository.UpdateSalary(id, salary);
        }

        /// <summary>
        /// This method returns an abstract summarized report of the current employees.
        /// </summary>
        /// <returns>The abstract report.</returns>
        public List<string> AbstractDetails()
        {
            List<string> details = new List<string>();

            details.Add($"Number of employees:\t{this.GetAll().Count()}");
            details.Add($"");
            details.Add($"Pilots ({this.GetAll().Where(x => x.EMPLOYEE_ID.ToString().StartsWith("2")).Count()} people)");
            foreach (var person in this.GetAll().Where(x => x.EMPLOYEE_ID.ToString().StartsWith("2")))
            {
                details.Add($"\t{person.NAME} since {person.HIRE_DATE.Value} with the revenue of {person.SALARY} EUR a year.");
            }

            details.Add($"Maintenance Crew ({this.GetAll().Where(x => x.EMPLOYEE_ID.ToString().StartsWith("4")).Count()} people)");
            foreach (var person in this.GetAll().Where(x => x.EMPLOYEE_ID.ToString().StartsWith("4")))
            {
                details.Add($"\t{person.NAME} since {person.HIRE_DATE.Value} with the revenue of {person.SALARY} EUR a year.");
            }

            details.Add($"Flight Attendants ({this.GetAll().Where(x => x.EMPLOYEE_ID.ToString().StartsWith("3")).Count()} people)");
            foreach (var person in this.GetAll().Where(x => x.EMPLOYEE_ID.ToString().StartsWith("3")))
            {
                details.Add($"\t{person.NAME} since {person.HIRE_DATE.Value} with the revenue of {person.SALARY} EUR a year.");
            }

            return details;
        }
    }
}
