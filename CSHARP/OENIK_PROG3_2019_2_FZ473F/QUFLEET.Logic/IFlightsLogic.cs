﻿// <copyright file="IFlightsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The FLIGHT table's CRUD operations are outsourced to this interface.
    /// </summary>
    public interface IFlightsLogic : ILogic<FLIGHT, int>
    {
        /// <summary>
        /// Creates a new flight route.
        /// </summary>
        /// <param name="departure">Departure airport.</param>
        /// <param name="arrival">Arrival airport.</param>
        /// <param name="type">Flight's class.</param>
        /// <param name="price">Flight price.</param>
        void CreateFlight(int departure, int arrival, string type, int price);

        /// <summary>
        /// Updates the selected flight's price.
        /// </summary>
        /// <param name="id">The flight's id.</param>
        /// <param name="price">The flight's new price.</param>
        void UpdatePrice(int id, int price);

        /// <summary>
        /// Deletes the selected flight from the FLIGHTS table.
        /// </summary>
        /// <param name="id">The flight's id.</param>
        void DeleteFlight(int id);
    }
}
