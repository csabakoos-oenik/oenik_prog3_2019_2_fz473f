﻿// <copyright file="FlightsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using QUFLEET.Data;
    using QUFLEET.Repository;

    /// <summary>
    /// This class contains the FLIGHT's table CRUD methods.
    /// </summary>
    public class FlightsLogic : IFlightsLogic
    {
        private FlightsRepository flightsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FlightsLogic"/> class.
        /// </summary>
        public FlightsLogic()
        {
            this.flightsRepository = new FlightsRepository();
        }

        /// <inheritdoc />
        public void CreateFlight(int departure, int arrival, string type, int price)
        {
            FLIGHT flight = new FLIGHT()
            {
                FLIGHT_ID = this.flightsRepository.GetAll().OrderByDescending(x => x.FLIGHT_ID).First().FLIGHT_ID + 1,
                FLEET_ID = this.flightsRepository.GetAll().First().FLEET_ID,
                DEPARTURE = departure,
                DEPART_DATE = DateTime.Now,
                ARRIVAL = arrival,
                ARRIVE_DATE = DateTime.Now,
                CLASS = type,
                PRICE = price,
            };

            this.flightsRepository.CreateFlight(flight);
        }

        /// <inheritdoc />
        public void DeleteFlight(int id)
        {
            this.flightsRepository.DeleteFlight(id);
        }

        /// <inheritdoc />
        public IQueryable<FLIGHT> GetAll()
        {
            return this.flightsRepository.GetAll();
        }

        /// <inheritdoc />
        public FLIGHT GetOne(int id)
        {
            return this.flightsRepository.GetOne(id);
        }

        /// <inheritdoc />
        public void UpdatePrice(int id, int price)
        {
            this.flightsRepository.UpdatePrice(id, price);
        }

        /// <summary>
        /// This method creates an XML file containing the available flight routes.
        /// </summary>
        public void FlightsToXML()
        {
            XDocument flightsXML = new XDocument();
            flightsXML.Add(new XElement("flights"));

            FlightsLogic flightsLogic = new FlightsLogic();

            var flightsQueryable = flightsLogic.GetAll();

            foreach (var item in flightsQueryable)
            {
                flightsXML.Root.Add(new XElement(
                    "flight",
                    new XElement("flight_id", item.FLIGHT_ID),
                    new XElement("fleet_id", item.FLEET_ID),
                    new XElement("departure", item.DEPARTURE),
                    new XElement("departure_date", item.DEPART_DATE),
                    new XElement("arrival", item.ARRIVAL),
                    new XElement("arrival_date", item.ARRIVE_DATE),
                    new XElement("class", item.CLASS),
                    new XElement("price", item.PRICE)));
            }

            flightsXML.Save("flights.xml");
        }
    }
}
