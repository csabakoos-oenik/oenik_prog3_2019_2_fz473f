﻿// <copyright file="IFleetLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The FLEET table's CRUD operations are outsourced to this interface.
    /// </summary>
    public interface IFleetLogic : ILogic<FLEET, string>
    {
        /// <summary>
        /// Adds a new plane to the fleet.
        /// </summary>
        /// <param name="callsign">The plane's callsign.</param>
        /// <param name="capacity">The plane's capacity.</param>
        /// <param name="model">The plane's model.</param>
        /// <param name="status">The plane's status.</param>
        void CreatePlane(string callsign, int capacity, string model, string status);

        /// <summary>
        /// Updates the selected plane's callsign.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <param name="callsign">The plane's new callsign.</param>
        void UpdateCallsign(string id, string callsign);

        /// <summary>
        /// Updates the selected plane's capacity.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <param name="capacity">The plane's new capacity.</param>
        void UpdateCapacity(string id, short capacity);

        /// <summary>
        /// Updates the selected plane's status.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <param name="status">The plane's new status.</param>
        void UpdateStatus(string id, string status);

        /// <summary>
        /// Deletes the selected plane from the FLEET table.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        void DeletePlane(string id);
    }
}
