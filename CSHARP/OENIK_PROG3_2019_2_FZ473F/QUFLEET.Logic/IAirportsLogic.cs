﻿// <copyright file="IAirportsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The AIRPORTS table's CRUD operations are outsourced to this interface.
    /// </summary>
    public interface IAirportsLogic : ILogic<AIRPORT, int>
    {
        /// <summary>
        /// This method creates a new airport.
        /// </summary>
        /// <param name="iata">The airport's IATA id.</param>
        /// <param name="icao">The airport's ICAO id.</param>
        /// <param name="faa">The airport's FAA id.</param>
        /// <param name="country">The airport's country.</param>
        /// <param name="city">The airport's city.</param>
        void CreateAirport(string iata, string icao, string faa, string country, string city);

        /// <summary>
        /// This method updates the given id's aiport's IATA id.
        /// </summary>
        /// <param name="id">The airport's id.</param>
        /// <param name="iata">The airport's new IATA.</param>
        void UpdateIATA(int id, string iata);

        /// <summary>
        /// This method updates the given id's aiport's ICAO id.
        /// </summary>
        /// <param name="id">The airport's id.</param>
        /// <param name="icao">The airport's new IATA.</param>
        void UpdateICAO(int id, string icao);

        /// <summary>
        /// This method updates the given id's aiport's FAA id.
        /// </summary>
        /// <param name="id">The airport's id.</param>
        /// <param name="faa">The airport's new IATA.</param>
        void UpdateFAA(int id, string faa);

        /// <summary>
        /// This method deletes the given id's airport.
        /// </summary>
        /// <param name="id">The airport's id.</param>
        void DeleteAirport(int id);
    }
}
