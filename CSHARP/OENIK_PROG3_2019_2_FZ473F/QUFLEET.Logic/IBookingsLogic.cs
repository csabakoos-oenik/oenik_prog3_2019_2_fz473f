﻿// <copyright file="IBookingsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The BOOKINGS table's CRUD operations are outsourced to this interface.
    /// </summary>
    public interface IBookingsLogic : ILogic<BOOKING, int>
    {
        /// <summary>
        /// Create a new booking and adds it to the BOOKINGS table.
        /// </summary>
        /// <param name="flight_id">Flight's id.</param>
        /// <param name="name">Name of the person.</param>
        void CreateBooking(int flight_id, string name);

        /// <summary>
        /// Deletes the selected booking from the BOOKINGS table.
        /// </summary>
        /// <param name="id">The booking's id.</param>
        void DeleteBooking(int id);
    }
}
