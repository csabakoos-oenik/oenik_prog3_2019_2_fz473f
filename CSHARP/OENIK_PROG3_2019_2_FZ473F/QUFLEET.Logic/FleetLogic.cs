﻿// <copyright file="FleetLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;
    using QUFLEET.Repository;

    /// <summary>
    /// This class contains the FLEET's table CRUD methods.
    /// </summary>
    public class FleetLogic : IFleetLogic
    {
        private FleetRepository fleetRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FleetLogic"/> class.
        /// </summary>
        public FleetLogic()
        {
            this.fleetRepository = new FleetRepository();
        }

        /// <inheritdoc/>
        public void CreatePlane(string callsign, int capacity, string model, string status)
        {
            FLEET plane = new FLEET()
            {
                FLEET_ID = "QU-" + callsign,
                CALLSIGN = "OE-" + callsign,
                CAPACITY = short.Parse(capacity.ToString()),
                MODEL = model,
                STATUS = status,
            };

            this.fleetRepository.CreatePlane(plane);
        }

        /// <inheritdoc/>
        public void DeletePlane(string id)
        {
            this.fleetRepository.DeletePlane(id);
        }

        /// <inheritdoc/>
        public FLEET GetOne(string id)
        {
            return this.fleetRepository.GetOne(id);
        }

        /// <inheritdoc/>
        public IQueryable<FLEET> GetAll()
        {
            return this.fleetRepository.GetAll();
        }

        /// <inheritdoc/>
        public void UpdateCallsign(string id, string callsign)
        {
            this.fleetRepository.UpdateCallsign(id, callsign);
        }

        /// <inheritdoc/>
        public void UpdateCapacity(string id, short capacity)
        {
            this.fleetRepository.UpdateCapacity(id, capacity);
        }

        /// <inheritdoc/>
        public void UpdateStatus(string id, string status)
        {
            this.fleetRepository.UpdateStatus(id, status);
        }

        /// <summary>
        /// This method returns the details of a plane.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <returns>The plain's details.</returns>
        public List<string> PlaneDetails(string id)
        {
            FlightsLogic flightsLogic = new FlightsLogic();
            AirportsLogic airportsLogic = new AirportsLogic();

            List<string> details = new List<string>();

            FLEET plane = this.GetOne(id);

            details.Add($"ID:\t\t{plane.FLEET_ID}");
            details.Add($"Callsign:\t{plane.CALLSIGN}");
            details.Add($"Manufacturer:\t{plane.MODEL.Split(' ')[0]}");
            details.Add($"Model:\t\t{plane.MODEL.Replace(plane.MODEL.Split(' ')[0] + " ", string.Empty)}");
            details.Add($"Capacity:\t{plane.CAPACITY}");
            details.Add($"Status:\t\t{plane.STATUS}");

            if (flightsLogic.GetAll().Where(x => x.FLEET_ID == plane.FLEET_ID).Count() > 0)
            {
                details.Add($"");
                details.Add($"The plane is currently active on {flightsLogic.GetAll().Where(x => x.FLEET_ID == plane.FLEET_ID).Count()} flight routes.");
                details.Add($"");
                details.Add($"The active route(s):");

                foreach (var route in flightsLogic.GetAll().Where(x => x.FLEET_ID == plane.FLEET_ID))
                {
                    details.Add($"\t{route.FLIGHT_ID} : {airportsLogic.GetAll().First(x => x.AIRPORT_ID == route.DEPARTURE).CITY} -> {airportsLogic.GetAll().First(x => x.AIRPORT_ID == route.ARRIVAL).CITY} ");
                }
            }
            else
            {
                details.Add($"");
                details.Add($"The plane is currently not on active duty.");
            }

            return details;
        }
    }
}
