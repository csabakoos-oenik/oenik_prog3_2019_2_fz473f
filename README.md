## OENIK_PROG3_2019_2_FZ473F

---

## About this repository

This repository is the *home* for my initial half-year project for the *[Webprogramming and advanced development techniques](http://users.nik.uni-obuda.hu/prog3/)* subject.

---

## About my project

My project itself will heavily depend on another half-year project which I'm doing for the *Databases* subject since both will use the same database, hierarchy and the *"business idea"* that is behind that.

The *Databases* project is around a fictional airline company called *QUJET Airline* and the whole idea behind that was to create a database which would store the company's fleet and employees details and also keeps track of flight routes (tickets), airfields, planes, and employees assignments individually.

This, the Webprogramming and advanced development techniques subject's project which I called *QUFLEET* will extend the previously mentioned Databases project by creating a C# Console Application that will be able to issue different queries and changes into the "same" database with the help of the Java web application and which will be able to give back the results of the issued commands in different outcomes using the learned methods to fulfill the *Business Logic* and *Layered Architecture* the project requires to have.

[Learn more about it here...](/CSHARP)

---

## Things to know about the project's requirements

The project has to be designed according to the layering principles of the semester, and also all the technologies have to be used that they learn throughout the semester. Everyone has to create a project work, even those who already completed a part of the subject.
The exact deadlines are announced by the practice teachers.
The created program has to implement some business tasks using a database that contains minimum three inter-connected tables.

**During the preparation of the project, the following expectations must be met:**

1. DoxyGen generated HTML/CHM/PDF documentation
2. The code must be FXCop/StyleCop validated, using the oenik.ruleset file
3. Usage of a database + Entity Framework to access it
4. Usage of LINQ
5. Code with unit tests (typically for the business logic classes)
6. Layered architecture (Business operations and data logic must be in separate layers from the UI, so typically minimum 4 projects: Console App + Business logic + Entities + Tests)
7. Single-user, single-branch GIT repository

---

## Gitstat knowledge ##

1. For MILESTONE1, you must have: JavaDirFound && CsharpDirFound && GitignoreFound && PackageDirsFound == 0

2. For MILESTONE2, you must have: MILESTONE1 && SlnFound == 1 && StylecopFound && ProjectsFound && DatabaseFound && BuildNumErrors == 0;

3. For MILESTONE3 (last milestone!!!), you must have: MILESTONE2 && PassedTests >= 8 && DoxygenFound && BuildJavaFound **IF MILESTONE3 IS FALSE, YOU CANNOT GET A GRADE**
	
4. The reason why you need min. 8 tests:

- You have minimum 3 entites, 5 crud methods (select all, select by id, update, delete, insert), 3 non crud methods
- No need to have an update for every single property, if all entity types are changeable SOMEHOW, that is enough
- Create one test per crud method type (5 tests: 1 select all, 1 select by id, 1 update, 1 insert, 1 delete; with Mock.Verify; spread out somehow to all 3 entities)
- Create one test per non-crud method (3 tests, with NUnit.Assert + Mock.Verify)
- All tests MUST USE mock! No tests can use the real database!

5. TSV files can still be opened with MS excel/Libreoffice calc...

6. It is FORBIDDEN to have Console.* outside the console app

7. GitStat is NO LONGER Azure hosted (managed to finally obtain a virtual machine, thanks to Dr. GĂĄbor KertĂŠsz :) ) ... Stats are (hopefully) automatically generated every day at 5am and 7pm CET (bugs might  intervene :) ). Expect that automatic results will be uploaded afterwards (current execution time for ~330 repositories is ~3 hours...)

8. For PackageDirsFound = 0, you must NOT have the Packages folder in your remote repository; it must only exist in your local repo. Should you have an error with this, these are the steps to fix:

- Quit from Visual Studio
- Manually remove the Packages folder from the solution
- Commit, push ( !!!IMPORTANT!!! Check on the Bitbucket website, there should be no Packages folder in the repository)
- Check .gitignore that it is correct (It should include a valid entry for the packages directory)
- Commit, push ( Check on the Bitbucket website, the good .gitignore should be shown)
- VS restart, rebuild solution
- When building, a "nuget restore" is executed, so the local Packages folder is added
- This local folder will NOT be part of the remote repository, it will not be a part of any new commits, the web should NEVER show any Packages folders in the repository
	
---

Contact szabo.zsolt@nik.uni-obuda.hu if you don't know why your data looks like the way it does.

---

**commits.tsv: commit statistics:**
*project + username + commit count*

**tests.tsv: project statistics:**
*Neptun code*
*Milestones OK/NOT OK*
*Milestone1 requirements: JavaDirFound, CsharpDirFound, GitignoreFound, PackageDirsFound*
*Milestone2 requirements: SlnFound, StylecopFound, ProjectsFound, DatabaseFound*
*Data from unit tests: test count + pass count + assertion count + opencover statistics (Sequence points, Branches, Classes, Methods)*
*Compiler output: Warnings, Errors*
*CLOC statistics *
*Other data*