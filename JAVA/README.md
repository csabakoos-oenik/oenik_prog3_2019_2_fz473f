## OENIK_PROG3_2019_2_FZ473F

---

## About this directory

The **JAVA** directory has been created and committed to the repository.

*This directory contains all of the Java program parts/files for the initial half year project for [Webprogramming and advanced development techniques](https://http://users.nik.uni-obuda.hu/prog3/) subject.*

---

## About the Java Web Application

The JAVA web application's main purpose is to support the created QUFLEET application.

**The main feature's details**

In the QUFLEET application customers are able to book flights to existing routes. (The announced flight routes have ids which are used in the booking process.) These flights all have a unique identifier in order to distinguish bookings. The Java Web Application's purpose is to provide the users a website where they can fill out a form in order to book a flight.