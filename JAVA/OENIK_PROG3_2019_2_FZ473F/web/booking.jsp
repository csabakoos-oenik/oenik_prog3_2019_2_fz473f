<%-- 
    Document   : booking
    Created on : Nov 23, 2019, 11:34:52 AM
    Author     : csaba
--%>

<%@page import="classes.FlightsList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Flight"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to the QUJET Airline's online booking platform!</title>
        <link rel="shortcut icon" href="https://cdn1.iconfinder.com/data/icons/all_google_icons_symbols_by_carlosjj-du/128/plane-o.png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    </head>
    <body style="background: url('images/flight-hero.jpg') no-repeat; background-size: cover; font-family: 'Open Sans', sans-serif; text-align: center;">
        <div style="margin: 0 auto; width: 80%;">
            <p style="text-align: center;"><img src="images/large_qujet_0.png" width="400"/> <img src="images/staralliance-logo.png" width="400" style="position: relative; top: -60px;" /></p>
            <h1 style="margin-top: 100px; text-align: center; color: #fff;">Here you can see the available flight routes</h1>
            <h3 style="color: #fff; text-align: center;">In order to book a flight please fill out the form below, where you have to give the chosen flight's ID along with your name.</h3>
            
            <form action="Book" method="POST">
                <br/>
                <span style="color: #fff; font-weight: bold;">Flight's ID:</span>
                <input type="text" name="booking_flight_id" value=""/>
                <span style="color: #fff; font-weight: bold;">Your name:</span>
                <input type="text" name="booking_name" value=""/>
                <br/>
                <br/>
                <input type="submit" name="booking_send" value="Book this flight"/>
            </form>
            
            <table cellspacing="0" collspacing="0" style="background: #fff; border: 1px solid #000; margin-top: 50px; padding: 0;" align="center">
                <tr bgcolor="#afafaf" style="font-weight: bold; margin: 0; padding: 0;">
                    <td style="padding: 10px 20px;">Flight's ID</td>
                    <td style="padding: 10px 20px;">Plane</td>
                    <td style="padding: 10px 20px;">Departure</td>
                    <td style="padding: 10px 20px;">Arrival</td>
                    <td style="padding: 10px 20px;">Departure Date</td>
                    <td style="padding: 10px 20px;">Arrival Date</td>
                    <td style="padding: 10px 20px;">Class</td>
                    <td style="padding: 10px 20px;">Price</td>
                </tr>
            
            <%
                FlightsList flightsList = new FlightsList();
                flightsList.Initialise();
                List<Flight> flights = flightsList.getFlights();
                
                int i = 0;
                
                for(Flight flight : flights)
                {
                    i++;
            %>
            
                <tr bgcolor='<%= i % 2 == 0 ? "#FFF" : "#F0F0F0"%>'>
                    <td style="padding: 10px;"><% out.write(String.valueOf(flight.flight_id)); %></td>
                    <td style="padding: 10px;"><% out.write(flight.fleet_id); %></td>
                    <td style="padding: 10px;"><% out.write(String.valueOf(flight.departure)); %></td>
                    <td style="padding: 10px;"><% out.write(String.valueOf(flight.arrival)); %></td>
                    <td style="padding: 10px;"><% out.write(flight.departure_date.toString()); %></td>
                    <td style="padding: 10px;"><% out.write(flight.arrival_date.toString()); %></td>
                    <td style="padding: 10px;"><% out.write(flight.type); %></td>
                    <td style="padding: 10px;"><% out.write(String.valueOf(flight.price)); %> €</td>
                </tr>
            
            <%
                }
            %>
            
            </table>
            
            <p style="margin: 50px; text-align: center;"><a href="index.html" style="text-decoration: none; color: #000; padding: 10px 20px; background: #fff;">Go back</a></p>
        </div>
    </body>
</html>
