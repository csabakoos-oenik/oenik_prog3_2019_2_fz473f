/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author csaba
 */
public class BookingSender {
    private String message;

    public BookingSender(String message) {
        this.message = message;
    }
    
    public void SendMessage() throws IOException{
        Socket socket = new Socket("localhost", 1034);
        
        PrintWriter pw = new PrintWriter(socket.getOutputStream());
        pw.print(message);
        pw.flush();
    }
}
