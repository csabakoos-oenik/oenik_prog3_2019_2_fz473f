/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.time.LocalDate;

/**
 *
 * @author csaba
 */
public class Flight {
    public int flight_id;
    public String fleet_id;
    public int departure;
    public LocalDate departure_date;
    public int arrival;
    public LocalDate arrival_date;
    public String type;
    public int price;

    public Flight(int flight_id, String fleet_id, int departure, int arrival, LocalDate departure_date, LocalDate arrival_date, String type, int price) {
        this.flight_id = flight_id;
        this.fleet_id = fleet_id;
        this.departure = departure;
        this.departure_date = departure_date;
        this.arrival = arrival;
        this.arrival_date = arrival_date;
        this.type = type;
        this.price = price;
    }

    public int getFlight_id() {
        return flight_id;
    }

    public String getFleet_id() {
        return fleet_id;
    }

    public int getDeparture() {
        return departure;
    }

    public LocalDate getDeparture_date() {
        return departure_date;
    }

    public int getArrival() {
        return arrival;
    }

    public LocalDate getArrival_date() {
        return arrival_date;
    }

    public String getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    public void setFlight_id(int flight_id) {
        this.flight_id = flight_id;
    }

    public void setFleet_id(String fleet_id) {
        this.fleet_id = fleet_id;
    }

    public void setDeparture(int departure) {
        this.departure = departure;
    }

    public void setDeparture_date(LocalDate departure_date) {
        this.departure_date = departure_date;
    }

    public void setArrival(int arrival) {
        this.arrival = arrival;
    }

    public void setArrival_date(LocalDate arrival_date) {
        this.arrival_date = arrival_date;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    
}
