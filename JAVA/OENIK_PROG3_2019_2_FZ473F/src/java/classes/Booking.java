/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author csaba
 */


public class Booking {
    public int booking_id;
    public int flight_id;
    public String name;

    public Booking(int booking_id, int flight_id, String name) {
        this.booking_id = booking_id;
        this.flight_id = flight_id;
        this.name = name;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public int getFlight_id() {
        return flight_id;
    }

    public String getName() {
        return name;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public void setFlight_id(int flight_id) {
        this.flight_id = flight_id;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
