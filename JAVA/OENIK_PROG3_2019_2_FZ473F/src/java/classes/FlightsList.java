/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author csaba
 */
public class FlightsList {
    private final List<Flight> flights;
    
    public FlightsList() {
        this.flights = new ArrayList<>();
    }
    
    public void Initialise(){
        AddFlights(new Flight(50001,"QU-A104",10012,10007,LocalDate.of(2020,9,4),LocalDate.of(2020,9,4),"BUSINESS",4500));
        AddFlights(new Flight(50002,"QU-BC10",10012,10007,LocalDate.of(2020,9,4),LocalDate.of(2020,9,4),"FIRST",6900));
        AddFlights(new Flight(50003,"QU-BC03",10045,10044,LocalDate.of(2020,6,12),LocalDate.of(2020,6,12),"BUSINESS",1400));
        AddFlights(new Flight(50004,"QU-BC09",10045,10044,LocalDate.of(2020,6,12),LocalDate.of(2020,6,12),"FIRST",7500));
        AddFlights(new Flight(50005,"QU-A304",10017,10038,LocalDate.of(2020,12,4),LocalDate.of(2020,12,4),"BUSINESS",5600));
        AddFlights(new Flight(50006,"QU-A802",10017,10038,LocalDate.of(2020,12,4),LocalDate.of(2020,12,4),"FIRST",500));
        AddFlights(new Flight(50007,"QU-BC09",10043,10033,LocalDate.of(2020,5,3),LocalDate.of(2020,5,3),"BUSINESS",1400));
        AddFlights(new Flight(50008,"QU-A801",10043,10033,LocalDate.of(2020,5,3),LocalDate.of(2020,5,3),"FIRST",6600));
        AddFlights(new Flight(50009,"QU-AN10",10032,10011,LocalDate.of(2020,1,17),LocalDate.of(2020,1,17),"BUSINESS",4400));
        AddFlights(new Flight(50010,"QU-CX13",10032,10011,LocalDate.of(2020,1,17),LocalDate.of(2020,1,17),"FIRST",5200));
        AddFlights(new Flight(50011,"QU-A106",10004,10012,LocalDate.of(2020,12,4),LocalDate.of(2020,12,4),"BUSINESS",2700));
        AddFlights(new Flight(50012,"QU-BC02",10004,10012,LocalDate.of(2020,12,4),LocalDate.of(2020,12,4),"FIRST",2300));
        AddFlights(new Flight(50013,"QU-BC10",10037,10013,LocalDate.of(2020,10,21),LocalDate.of(2020,10,21),"BUSINESS",6300));
        AddFlights(new Flight(50014,"QU-BC08",10037,10013,LocalDate.of(2020,10,21),LocalDate.of(2020,10,21),"FIRST",4500));
        AddFlights(new Flight(50015,"QU-AN04",10026,10047,LocalDate.of(2020,9,9),LocalDate.of(2020,9,9),"BUSINESS",3100));
        AddFlights(new Flight(50016,"QU-AN01",10026,10047,LocalDate.of(2020,9,9),LocalDate.of(2020,9,9),"FIRST",8200));
        AddFlights(new Flight(50017,"QU-A102",10012,10031,LocalDate.of(2020,8,24),LocalDate.of(2020,8,24),"BUSINESS",2900));
        AddFlights(new Flight(50018,"QU-CX07",10012,10031,LocalDate.of(2020,8,24),LocalDate.of(2020,8,24),"FIRST",4100));
        AddFlights(new Flight(50019,"QU-CX11",10001,10041,LocalDate.of(2020,10,2),LocalDate.of(2020,10,2),"BUSINESS",3600));
        AddFlights(new Flight(50020,"QU-BC10",10015,10042,LocalDate.of(2020,4,26),LocalDate.of(2020,4,26),"BUSINESS",4300));
        AddFlights(new Flight(50021,"QU-A301",10015,10042,LocalDate.of(2020,4,26),LocalDate.of(2020,4,26),"FIRST",8900));
        AddFlights(new Flight(50022,"QU-CX03",10042,10013,LocalDate.of(2020,9,4),LocalDate.of(2020,9,4),"BUSINESS",800));
        AddFlights(new Flight(50023,"QU-A303",10042,10013,LocalDate.of(2020,9,4),LocalDate.of(2020,9,4),"FIRST",3900));
        AddFlights(new Flight(50024,"QU-A802",10025,10009,LocalDate.of(2020,1,20),LocalDate.of(2020,1,20),"BUSINESS",9000));
        AddFlights(new Flight(50025,"QU-AN10",10025,10009,LocalDate.of(2020,1,20),LocalDate.of(2020,1,20),"FIRST",7700));
        AddFlights(new Flight(50026,"QU-AN10",10046,10024,LocalDate.of(2020,5,24),LocalDate.of(2020,5,24),"BUSINESS",2500));
        AddFlights(new Flight(50027,"QU-A304",10046,10024,LocalDate.of(2020,5,24),LocalDate.of(2020,5,24),"FIRST",3000));
        AddFlights(new Flight(50028,"QU-CX11",10034,10042,LocalDate.of(2020,1,17),LocalDate.of(2020,1,17),"BUSINESS",6600));
        AddFlights(new Flight(50029,"QU-A801",10034,10042,LocalDate.of(2020,1,17),LocalDate.of(2020,1,17),"FIRST",8900));
        AddFlights(new Flight(50030,"QU-A102",10016,10024,LocalDate.of(2020,9,20),LocalDate.of(2020,9,20),"BUSINESS",6600));
        AddFlights(new Flight(50031,"QU-BC10",10016,10024,LocalDate.of(2020,9,20),LocalDate.of(2020,9,20),"FIRST",2000));
        AddFlights(new Flight(50032,"QU-A802",10047,10019,LocalDate.of(2020,2,5),LocalDate.of(2020,2,5),"BUSINESS",1600));
        AddFlights(new Flight(50033,"QU-A801",10047,10019,LocalDate.of(2020,2,5),LocalDate.of(2020,2,5),"FIRST",1300));
        AddFlights(new Flight(50034,"QU-A802",10009,10023,LocalDate.of(2020,11,18),LocalDate.of(2020,11,18),"BUSINESS",9400));
        AddFlights(new Flight(50035,"QU-AN10",10009,10023,LocalDate.of(2020,11,18),LocalDate.of(2020,11,18),"FIRST",3600));
        AddFlights(new Flight(50036,"QU-AN10",10023,10011,LocalDate.of(2020,10,4),LocalDate.of(2020,10,4),"BUSINESS",7300));
        AddFlights(new Flight(50037,"QU-BC05",10023,10011,LocalDate.of(2020,10,4),LocalDate.of(2020,10,4),"FIRST",7800));
        AddFlights(new Flight(50038,"QU-A304",10038,10017,LocalDate.of(2020,4,25),LocalDate.of(2020,4,25),"BUSINESS",2600));
        AddFlights(new Flight(50039,"QU-BC10",10026,10004,LocalDate.of(2020,8,21),LocalDate.of(2020,8,21),"BUSINESS",3600));
        AddFlights(new Flight(50040,"QU-CX11",10026,10004,LocalDate.of(2020,8,21),LocalDate.of(2020,8,21),"FIRST",6300));
        AddFlights(new Flight(50041,"QU-A801",10042,10034,LocalDate.of(2020,10,27),LocalDate.of(2020,10,27),"BUSINESS",4000));
        AddFlights(new Flight(50042,"QU-CX12",10042,10034,LocalDate.of(2020,10,27),LocalDate.of(2020,10,27),"FIRST",7800));
        AddFlights(new Flight(50043,"QU-A802",10002,10007,LocalDate.of(2020,9,26),LocalDate.of(2020,9,26),"BUSINESS",4200));
        AddFlights(new Flight(50044,"QU-A104",10002,10007,LocalDate.of(2020,9,26),LocalDate.of(2020,9,26),"FIRST",4400));
        AddFlights(new Flight(50045,"QU-A802",10032,10036,LocalDate.of(2020,8,18),LocalDate.of(2020,8,18),"BUSINESS",1600));
        AddFlights(new Flight(50046,"QU-A301",10032,10036,LocalDate.of(2020,8,18),LocalDate.of(2020,8,18),"FIRST",1700));
        AddFlights(new Flight(50047,"QU-CX01",10031,10015,LocalDate.of(2020,9,10),LocalDate.of(2020,9,10),"BUSINESS",6300));
        AddFlights(new Flight(50048,"QU-A302",10031,10015,LocalDate.of(2020,9,10),LocalDate.of(2020,9,10),"FIRST",5700));
        AddFlights(new Flight(50049,"QU-A802",10036,10039,LocalDate.of(2020,4,7),LocalDate.of(2020,4,7),"BUSINESS",6700));
        AddFlights(new Flight(50050,"QU-BC10",10036,10039,LocalDate.of(2020,4,7),LocalDate.of(2020,4,7),"FIRST",6500));
        AddFlights(new Flight(50051,"QU-AN10",10014,10019,LocalDate.of(2020,7,22),LocalDate.of(2020,7,22),"BUSINESS",9000));
        AddFlights(new Flight(50052,"QU-A105",10014,10019,LocalDate.of(2020,7,22),LocalDate.of(2020,7,22),"FIRST",1900));
        AddFlights(new Flight(50053,"QU-A304",10038,10045,LocalDate.of(2020,11,26),LocalDate.of(2020,11,26),"BUSINESS",4800));
        AddFlights(new Flight(50054,"QU-AN10",10038,10045,LocalDate.of(2020,11,26),LocalDate.of(2020,11,26),"FIRST",6200));
        AddFlights(new Flight(50055,"QU-BC04",10009,10031,LocalDate.of(2020,2,4),LocalDate.of(2020,2,4),"BUSINESS",4700));
        AddFlights(new Flight(50056,"QU-A301",10009,10031,LocalDate.of(2020,2,4),LocalDate.of(2020,2,4),"FIRST",9700));
        AddFlights(new Flight(50057,"QU-BC10",10046,10016,LocalDate.of(2020,11,10),LocalDate.of(2020,11,10),"BUSINESS",800));
        AddFlights(new Flight(50058,"QU-A801",10047,10013,LocalDate.of(2020,5,13),LocalDate.of(2020,5,13),"BUSINESS",1200));
        AddFlights(new Flight(50059,"QU-CX13",10047,10013,LocalDate.of(2020,5,13),LocalDate.of(2020,5,13),"FIRST",6100));
        AddFlights(new Flight(50060,"QU-AN01",10041,10009,LocalDate.of(2020,6,12),LocalDate.of(2020,6,12),"BUSINESS",2100));
        AddFlights(new Flight(50061,"QU-AN04",10041,10009,LocalDate.of(2020,6,12),LocalDate.of(2020,6,12),"FIRST",5600));
        AddFlights(new Flight(50062,"QU-AN10",10044,10001,LocalDate.of(2020,2,18),LocalDate.of(2020,2,18),"BUSINESS",9500));
        AddFlights(new Flight(50063,"QU-BC07",10044,10001,LocalDate.of(2020,2,18),LocalDate.of(2020,2,18),"FIRST",3700));
        AddFlights(new Flight(50064,"QU-A102",10035,10040,LocalDate.of(2020,1,1),LocalDate.of(2020,1,1),"BUSINESS",5600));
        AddFlights(new Flight(50065,"QU-A801",10035,10040,LocalDate.of(2020,1,1),LocalDate.of(2020,1,1),"FIRST",1500));
        AddFlights(new Flight(50066,"QU-AN10",10029,10020,LocalDate.of(2020,9,28),LocalDate.of(2020,9,28),"BUSINESS",1500));
        AddFlights(new Flight(50067,"QU-CX11",10029,10020,LocalDate.of(2020,9,28),LocalDate.of(2020,9,28),"FIRST",3000));
        AddFlights(new Flight(50068,"QU-CX11",10004,10013,LocalDate.of(2020,11,15),LocalDate.of(2020,11,15),"BUSINESS",7000));
        AddFlights(new Flight(50069,"QU-A304",10004,10013,LocalDate.of(2020,11,15),LocalDate.of(2020,11,15),"FIRST",2700));
        AddFlights(new Flight(50070,"QU-BC10",10008,10028,LocalDate.of(2020,2,6),LocalDate.of(2020,2,6),"BUSINESS",2300));
        AddFlights(new Flight(50071,"QU-CX11",10008,10028,LocalDate.of(2020,2,6),LocalDate.of(2020,2,6),"FIRST",3100));
        AddFlights(new Flight(50072,"QU-BC02",10020,10006,LocalDate.of(2020,1,27),LocalDate.of(2020,1,27),"BUSINESS",2500));
        AddFlights(new Flight(50073,"QU-CX05",10020,10006,LocalDate.of(2020,1,27),LocalDate.of(2020,1,27),"FIRST",2200));
        AddFlights(new Flight(50074,"QU-BC08",10022,10038,LocalDate.of(2020,11,13),LocalDate.of(2020,11,13),"BUSINESS",9600));
        AddFlights(new Flight(50075,"QU-CX13",10022,10038,LocalDate.of(2020,11,13),LocalDate.of(2020,11,13),"FIRST",1300));
        AddFlights(new Flight(50076,"QU-BC10",10040,10038,LocalDate.of(2020,8,18),LocalDate.of(2020,8,18),"BUSINESS",7100));
        AddFlights(new Flight(50077,"QU-AN10",10030,10012,LocalDate.of(2020,2,22),LocalDate.of(2020,2,22),"BUSINESS",3500));
        AddFlights(new Flight(50078,"QU-A103",10030,10012,LocalDate.of(2020,2,22),LocalDate.of(2020,2,22),"FIRST",3700));
        AddFlights(new Flight(50079,"QU-A105",10032,10023,LocalDate.of(2020,3,22),LocalDate.of(2020,3,22),"BUSINESS",2400));
        AddFlights(new Flight(50080,"QU-AN10",10032,10023,LocalDate.of(2020,3,22),LocalDate.of(2020,3,22),"FIRST",6600));
        AddFlights(new Flight(50081,"QU-A101",10041,10038,LocalDate.of(2020,7,15),LocalDate.of(2020,7,15),"BUSINESS",8800));
        AddFlights(new Flight(50082,"QU-A801",10041,10038,LocalDate.of(2020,7,15),LocalDate.of(2020,7,15),"FIRST",4300));
        AddFlights(new Flight(50083,"QU-AN06",10041,10016,LocalDate.of(2020,8,20),LocalDate.of(2020,8,20),"BUSINESS",1900));
        AddFlights(new Flight(50084,"QU-CX03",10041,10016,LocalDate.of(2020,8,20),LocalDate.of(2020,8,20),"FIRST",8100));
        AddFlights(new Flight(50085,"QU-CX11",10029,10027,LocalDate.of(2020,11,12),LocalDate.of(2020,11,12),"BUSINESS",1100));
        AddFlights(new Flight(50086,"QU-AN10",10029,10027,LocalDate.of(2020,11,12),LocalDate.of(2020,11,12),"FIRST",5000));
        AddFlights(new Flight(50087,"QU-CX01",10023,10011,LocalDate.of(2020,9,22),LocalDate.of(2020,9,22),"BUSINESS",3500));
        AddFlights(new Flight(50088,"QU-AN06",10023,10011,LocalDate.of(2020,9,22),LocalDate.of(2020,9,22),"FIRST",5000));
        AddFlights(new Flight(50089,"QU-A301",10042,10021,LocalDate.of(2020,8,11),LocalDate.of(2020,8,11),"BUSINESS",3400));
        AddFlights(new Flight(50090,"QU-A106",10042,10021,LocalDate.of(2020,8,11),LocalDate.of(2020,8,11),"FIRST",6400));
        AddFlights(new Flight(50091,"QU-A802",10030,10034,LocalDate.of(2020,8,6),LocalDate.of(2020,8,6),"BUSINESS",2800));
        AddFlights(new Flight(50092,"QU-CX05",10030,10034,LocalDate.of(2020,8,6),LocalDate.of(2020,8,6),"FIRST",900));
        AddFlights(new Flight(50093,"QU-BC10",10021,10022,LocalDate.of(2020,9,2),LocalDate.of(2020,9,2),"BUSINESS",2400));
        AddFlights(new Flight(50094,"QU-A105",10021,10022,LocalDate.of(2020,9,2),LocalDate.of(2020,9,2),"FIRST",2100));
    }
    
    private void AddFlights(Flight flight){
        flights.add(flight);
    }

    public List<Flight> getFlights() {
        return flights;
    }
}
